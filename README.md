# INTRODUCCION

## PROBLEMATICA

Un gran porcentaje de inicial y primaria enfrenta dificultades significativas para aprender matemáticas, lo que puede afectar su rendimiento académico y su actitud hacia la materia a largo plazo.

## MOTIVACIÓN

Nuestra primera motivación se basa en querer promover que las demás personas aprendan matemáticas mediante sumas, restas y ecuaciones a través de juegos interactivos.

Nuestra segunda motivación radica en demostrar nuestras habilidades y conocimientos adquiridos a lo largo del curso.

## SOLUCION

Crear un programa educativo interactivo destinado a enseñar de manera efectiva los conceptos de las operaciones básicas de suma, resta y ecuaciones con una incógnita, con el objetivo de facilitar el aprendizaje de estos conceptos importantes para los estudiantes.
 
# DESARROLLO

## REQUISITOS FUNCIONALES

| ID | Requisito |
| ------ | ------ |
| R001       |Implementar una pantalla con el logo y nombre del juego al ejecutar el programa        |
| R002       |Implementar una pantalla interactuable con opciones para iniciar partida, ver el ranking y salir         |
| R003       |Implementar una pantalla con las instrucciones del juego        |
| R004       |Implementar una pantalla con los creditos a los creadores         |
| R005       |Implementar una pantalla que solicite el nombre del jugador al iniciar partida       |
| R006       |Implementar una pantalla de ranking con los 20 mejores puntajes    |
| R007       |Implementar una pantalla de confirmacion de cierre del programa        |
| R008       |Implementar una Scoreboard con el nombre de usuario, vidas restantes, tiempo restante, progreso de aprendizaje, puntaje y objetivo          |
| R009       |Implementar un mapa sobre ecuaciones de suma en el mundo uno     |
| R010       |Implementar un mapa sobre ecuaciones de resta en el mundo dos        |
| R011       |Implementar un mapa sobre ecuaciones con incognitas en el mundo tres        |
| R012       |Implementar una funcion para controlar a un personaje        |
| R013       |Implementar una funcion para mostrar a los tutores      |
| R014       |Implementar una funcion para mover aleatoriamente a los enemigos        |
| R015       |Implementar una funcion para controlar a un personaje        |
| R016       |Implementar una funcion para interactuar con un tutor      |
| R017       |Implementar una funcion para interactuar con un enemigo      |
| R018       |Implementar una pantalla de victoria al ganar la partida   |
| R019       |Implementar una pantalla de derrota al perder todas las vidas      |

## MODULO DE PROGRAMACIÓN

| MÓDULOS | FUNCIONALIDAD | ENCARGADO |
| ------ | ------ | ------ |
| Módulo 1       | Gestionar los mundos y el movimiento del jugador      | Leonardo Rafael Garcia Villanueva       |
| Módulo 2       | Gestionar las mecánicas de los enemigos, tutores y portales       | Julio Frank Quispe Serrano       |
| Módulo 3       | Gestionar la pantalla de opciones cuando se ejecuta el programa y las estructuras de las entidades del juego   | Adriano Samir Centeno León       |

# MECÁNICAS, DINÁMICAS Y SECUENCIAS

1. El personaje se mueve en direcciones de arriba, izquierda, abajo y derecha con las teclas W, A, S, D respectivamente.
2. En el mapa habrán portales donde el personaje se teletransportará de un punto a otro al tener contacto con dicho portal.
3. El jugador debe pasar por laberintos en donde el tutor le da informacion y además le brinda un item que hará frente a los enemigos.
4. Si el jugador choca con el enemigo, entonces este le preguntará un problema y avanzará sin problema; si no tiene el item, pierde una vida y vuelve a aparecer al inicio del mundo.
5. Cuando logra pasar el laberinto y derrotar todos los enemigos, el jugador pasa al siguiente mundo. Si el jugador pierde todas sus vidas el jugador muere y regresa al lugar de inicio.


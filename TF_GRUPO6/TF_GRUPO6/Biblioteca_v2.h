#ifndef __BIBLIOTECA_V2_H__
#define __BIBLIOTECA_V2_H__

#include <iostream>
#include <conio.h>
#include <ctime>
#include <cmath>	
#include <string>
#include <iomanip>

using namespace std;
using namespace System;

const char O = 219;
const int LONGITUD = 120;
const int ALTITUD = 30;

/*------- Estructuras a usarse -------*/
struct Partida_t {
	int vidas_restantes;
	bool* mundo_superado;
	int progreso_aprendizaje;
};  
struct Jugador_t {
	int pos_x;
	int pos_y;
	int direccion;
	bool* items_recibidos;
};
struct Tutor_t {
	int pos_x[3];
	int pos_y[3];
	short* interaccion;
};
struct Enemigo_t {
	int pos_x[3];
	int pos_y[3];
	int* mov_x;
	int* mov_y;
	bool* mov_max;
	short* interaccion;
	short* vencido;
};
struct Portal_t {
	int pos_x[2];
	int pos_y[2];
};
struct Meta_t {
	int pos_x;
	int pos_y;
};
struct Scoreboard_t {
	bool imprimir;
	int tiempo;
	int mundo;
};

/*-----------------------------DECLARACION DE FUNCIONES-----------------------------*/

//Funcion constante
void imprimirMarco();

//Funcion interaccion humano-computadora
void keyboardHandle(int& tecla);

//Funcion para colores
void asignarColor(int color);

//Funciones de ventanas iniciales
void mensajeInicio();
void ventanaInicio();
void ventanaHistoria();
void Historia();
void ventanaInstrucciones();
void InstruccionesPaginaUno();
void InstruccionesPaginaDos();
void ventanaInteractiva(int& opcion_actual);
void ventanaNuevaPartida(string* NombresDeUsuarios, int& partida_actual);
void ventanaCreadores();
void ventanaSalir(bool& salida);

// Ranking
void OrdenarRanking(string* NombresDeUsuarios, int* PuntajesDeUsuarios);
void ventanaRankings(string* NombresDeUsuarios, int* PuntajesDeUsuarios);

//Juego
void PartidaEnJuego(int& PuntajesDeUsuarios, string NombresDeUsuarios);

//Mundos
void PintarMundo(short Mundo[24][120], int y, int x);
void ImprimirMundo(short Mundo[24][120]);
void MundoUno(Partida_t& Partida, int& PuntajesDeUsuarios, string NombresDeUsuarios);
void MundoDos(Partida_t& Partida, int& PuntajesDeUsuarios, string NombresDeUsuarios);
void MundoTres(Partida_t& Partida, int& PuntajesDeUsuarios, string NombresDeUsuarios);

//Mensajes de tutores y enemigos del mundo 1, ecuaciones de suma
void Msg_Tutor1_Mundo1();
void Msg_Tutor2_Mundo1();
void Msg_Tutor3_Mundo1();
bool Msg_Enemigo1_Mundo1(int var1, int var2, int respuesta);
bool Msg_Enemigo2_Mundo1(int var1, int var2, int respuesta);
bool Msg_Enemigo3_Mundo1(int var1, int var2, int respuesta);

//Mensajes de tutores y enemigos del mundo 2, ecuaciones de resta
void Msg_Tutor1_Mundo2();
void Msg_Tutor2_Mundo2();
void Msg_Tutor3_Mundo2();
bool Msg_Enemigo1_Mundo2(int var1, int var2, int respuesta);
bool Msg_Enemigo2_Mundo2(int var1, int var2, int respuesta);
bool Msg_Enemigo3_Mundo2(int var1, int var2, int respuesta);

//Mensajes de tutores y enemigos del mundo 3, ecuaciones con una incognita
void Msg_Tutor1_Mundo3();
void Msg_Tutor2_Mundo3();
void Msg_Tutor3_Mundo3();
bool Msg_Enemigo1_Mundo3(int var1, int var2, int respuesta);
bool Msg_Enemigo2_Mundo3(int var1, int var2, int respuesta);
bool Msg_Enemigo3_Mundo3(int var1, int var2, int var3, int respuesta);

//Scoreboard
void ScoreBoard(Partida_t Partida, Scoreboard_t& Scoreboard, int PuntajesDeUsuarios, string NombresDeUsuarios);
void LimpiarMensajeScoreboard();

//Funciones del jugador
bool ColisionJugadorMapa(Jugador_t Jugador, short Mundo[24][120]);
bool ColisionJugadorTutor(Jugador_t Jugador, Tutor_t& Tutor);
bool ColisionJugadorEnemigo(Jugador_t Jugador, Enemigo_t& Enemigo);
bool ColisionJugadorMeta(Jugador_t Jugador, Meta_t Meta, bool& mundo_superado, int mundo_actual);
void LimpiarJugador(Jugador_t Jugador, short Mundo[24][120]);
void MovimientoJugador(Jugador_t& Jugador, Tutor_t& Tutor, Enemigo_t& Enemigo, Meta_t Meta,
					   short Mundo[24][120], bool& mundo_superado, int mundo_actual);
void ImprimirJugador(Jugador_t& Jugador, short Mundo[24][120]);

//Funciones del enemigo
void ColisionEnemigoJugador(Jugador_t& Jugador, Enemigo_t& Enemigo);
void InteraccionEnemigos_Mundo1(Jugador_t& Jugador, Enemigo_t& Enemigo, Partida_t& Partida, int& PuntajesDeUsuarios, short Mundo[24][120]);
void InteraccionEnemigos_Mundo2(Jugador_t& Jugador, Enemigo_t& Enemigo, Partida_t& Partida, int& PuntajesDeUsuarios, short Mundo[24][120]);
void InteraccionEnemigos_Mundo3(Jugador_t& Jugador, Enemigo_t& Enemigo, Partida_t& Partida, int& PuntajesDeUsuarios, short Mundo[24][120]);
void LimpiarEnemigo(Enemigo_t& Enemigo, short Mundo[24][120]);
void MovimientoEnemigo_Mundo1(Enemigo_t& Enemigo);
void MovimientoEnemigo_Mundo2(Enemigo_t& Enemigo);
void MovimientoEnemigo_Mundo3(Enemigo_t& Enemigo);
void ImprimirEnemigo(Enemigo_t Enemigo, short Mundo[24][120]);

//Funciones del tutor
void ImprimirTutor(Tutor_t Tutor, short Mundo[24][120]);
void InteraccionTutores_Mundo1(Jugador_t& Jugador, Tutor_t& Tutor, int& progreso_aprendizaje, int& PuntajesDeUsuarios);
void InteraccionTutores_Mundo2(Jugador_t& Jugador, Tutor_t& Tutor, int& progreso_aprendizaje, int& PuntajesDeUsuarios);
void InteraccionTutores_Mundo3(Jugador_t& Jugador, Tutor_t& Tutor, int& progreso_aprendizaje, int& PuntajesDeUsuarios);

//Funcion adicional de portal
void ImprimirPortal(Portal_t Portal, short Mundo[24][120]);
void FuncionAdicionalPortal(Jugador_t& Jugador, Portal_t Portal, short Mundo[24][120], int mundo_actual);

//Funciones end-game
void ventanaGanaste(int PuntajesDeUsuarios);
void ventanaPerdiste(int PuntajesDeUsuarios);

/*-----------------------------FUNCIONES CON CONTENIDO-----------------------------*/

//Funcion constante
void imprimirMarco() {

	const char EsquinaSuperiorIzquierda = 201;
	const char EsquinaSuperiorDerecha = 187;
	const char EsquinaInferiorIzquierda = 200;
	const char EsquinaInferiorDerecha = 188;
	const char ConectorHorizontal = 205;
	const char ConectorVertical = 186;

	//Marco inicial
	Console::SetCursorPosition(0, 0); cout << EsquinaSuperiorIzquierda;
	Console::SetCursorPosition(119, 0); cout << EsquinaSuperiorDerecha;

	Console::SetCursorPosition(0, 29); cout << EsquinaInferiorIzquierda;
	Console::SetCursorPosition(119, 29); cout << EsquinaInferiorDerecha;

	for (int i = 1; i < 119; ++i) {
		Console::SetCursorPosition(i, 0); cout << ConectorHorizontal;
		Console::SetCursorPosition(i, 29); cout << ConectorHorizontal;
	}

	for (int i = 1; i < 29; ++i) {
		Console::SetCursorPosition(0, i); cout << ConectorVertical;
		Console::SetCursorPosition(119, i); cout << ConectorVertical;
	}
}

//Funcion interaccion humano - computadora
void keyboardHandle(int& tecla) {

	char key = 0;
	tecla = 0;
	if (_kbhit()) {
		key = toupper(_getch());
		switch (key) {
		case 'W':case (char)72: 
			tecla = 1; 
			break;
		case 'A':case (char)75: 
			tecla = 2; 
			break;
		case 'S':case (char)80: 
			tecla = 3; 
			break;
		case 'D':case (char)77: 
			tecla = 4;
			break;
		case (char)13: 
			tecla = 5; 
			break;
		}
	}
}

//Funcion para colores
void asignarColor(int color) {

	switch (color) {
	case 0:
		Console::ForegroundColor = ConsoleColor::Black;
		break;
	case 1:
		Console::ForegroundColor = ConsoleColor::Cyan;
		break;
	case 2:
		Console::ForegroundColor = ConsoleColor::DarkBlue;
		break;
	case 3:
		Console::ForegroundColor = ConsoleColor::DarkCyan;
		break;
	case 4:
		Console::ForegroundColor = ConsoleColor::DarkGray;
		break;
	case 5:
		Console::ForegroundColor = ConsoleColor::DarkMagenta;
		break;
	case 6:
		Console::ForegroundColor = ConsoleColor::DarkRed;
		break;
	case 7:
		Console::ForegroundColor = ConsoleColor::DarkYellow;
		break;
	case 8:
		Console::ForegroundColor = ConsoleColor::Magenta;
		break;
	case 9:
		Console::ForegroundColor = ConsoleColor::Red;
		break;
	case 10:
		Console::ForegroundColor = ConsoleColor::White;
		break;
	case 11:
		Console::ForegroundColor = ConsoleColor::Yellow;
		break;
	case 12:
		Console::ForegroundColor = ConsoleColor::Gray;
		break;
	case 13:
		Console::ForegroundColor = ConsoleColor::Blue;
		break;
	case 14:
		Console::ForegroundColor = ConsoleColor::DarkGreen;
		break;
	case 15:
		Console::ForegroundColor = ConsoleColor::Green;
		break;
	}
}

//Funciones de ventanas iniciales
void mensajeInicio() {
	//Nombre del juego
	bool nombre[10][97] = { {0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
							{0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1},
							{0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1},
							{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1},
							{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1},
							{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1},
							{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1},
							{0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1},
							{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
							{1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1} };

	//Logo del juego
	bool logo[8][18] = { {0,0,0,0,0,0,0,0,1,1,1,1,1,1,1,1,1,1},
						 {0,0,0,0,1,1,0,0,1,1,0,0,0,0,0,0,1,1},
						 {0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,1,1},
						 {1,1,1,1,1,1,1,1,1,1,0,0,1,1,1,1,1,1},
						 {0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0},
						 {0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0},
						 {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
						 {1,1,1,1,1,1,1,1,1,1,0,0,1,1,0,0,0,0} };

	//Dando color
	asignarColor(11);

	//Impresion del nombre
	for (int i = 0; i < 10; ++i) {
		for (int j = 0; j < 97; ++j) {
			if (nombre[i][j] == 0) {
				Console::SetCursorPosition(j + 12, i + 2); cout << O;
			}
		}
	}

	//Impresion del logo
	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 18; ++j) {
			if (logo[i][j] == 1) {
				Console::SetCursorPosition(j + 51, i + 13); cout << O;
			}
		}
	}
	
	//Color por defecto
	asignarColor(12);
}
void ventanaInicio() {

	mensajeInicio();

	//Esperando interaccion con el teclado
	for (;;) {

		_sleep(200);
		Console::SetCursorPosition(42, 24); cout << "- PRESIONA ESPACIO PARA EMPEZAR -";
		_sleep(200);
		Console::SetCursorPosition(42, 24); cout << "                                 ";

		if (_kbhit()) { 
			char key = _getch();
			if (key == (char)32) {
				break;
			}
		}
	}

}
void ventanaHistoria() {

	int tecla = 0;

	Console::Clear();
	imprimirMarco();

	Historia();

	//Bucle de impresiones manejado por keyboardHandle
	for (;;) {

		keyboardHandle(tecla);
		if (tecla == 5) { break; }

	}

}
void Historia() {

	Console::Clear();
	imprimirMarco();

	asignarColor(12);
	Console::SetCursorPosition(53, 1);
	cout << "H I S T O R I A";

	asignarColor(11);
	Console::SetCursorPosition(15, 3); cout << "Habia una vez un personaje, cuya sed de conocimiento era insaciable. Este personaje,";
	Console::SetCursorPosition(15, 4); cout << "se embarco en un viaje por el mundo en mundo en busqueda de sabiduria y entendimiento.";
	Console::SetCursorPosition(15, 5); cout << "Pero su viaje lo llevara a vastas llanuras, densas sabanas y altas monta�as gelidas.";
	Console::SetCursorPosition(15, 6); cout << "En su busqueda se encontrara con sabios ancestrales que le compartiran conocimientos que";
	Console::SetCursorPosition(15, 7); cout << "trascienden el tiempo y el espacio. Estos sabios encomendaran al personaje a llegar a tres";
	Console::SetCursorPosition(15, 8); cout << "ciudades a repartir conocimiento.";

	Console::SetCursorPosition(15, 10); cout << "En primer lugar, conocio a Elio quien compartio con el personaje los secretos de la suma.";
	Console::SetCursorPosition(15, 11); cout << "En segundo lugar, el personaje conocio a Maya, quien le ense�o los secretos de la resta.";
	Console::SetCursorPosition(15, 12); cout << "En tercer lugar, encontro a Zenon, este compartio a nuestro personaje los secretos de las";
	Console::SetCursorPosition(15, 13); cout << "ecuaciones de suma y resta con incognitas, cuya resolucion requeria agudeza mental";
	Console::SetCursorPosition(15, 14); cout << "y destreza numerica. Cada uno de estos sabios le pidio al personaje que compartiera";
	Console::SetCursorPosition(15, 15); cout << "este saber con aquellos que buscaban mas conocimiento en las tres Ciudades de los";
	Console::SetCursorPosition(15, 16); cout << "Conocimientos.";

	Console::SetCursorPosition(15, 18); cout << "En su travesia, el personaje se encontro con un grupo peligroso conocidos como los";
	Console::SetCursorPosition(15, 19); cout << "Ladrones del Saber, que intentaban robar los conocimientos que el habia adquirido";
	Console::SetCursorPosition(15, 20); cout << "de los sabios. Para protegerse de ellos, el personaje tuvo que poner a prueba sus";
	Console::SetCursorPosition(15, 21); cout << "habilidades matematicas resolviendo acertijos matematicos dados por los ladrones";

	Console::SetCursorPosition(15, 23); cout << "Con tenacidad y astucia, el personaje logro vencer a los Ladrones del Saber y llegar";
	Console::SetCursorPosition(15, 24); cout << "finalmente a cada Ciudad de los Conocimientos. Alli, compartio con humildad todo lo";
	Console::SetCursorPosition(15, 25); cout << "aprendido en su viaje, honrando la memoria de los sabios que lo habian guiado y ";
	Console::SetCursorPosition(15, 26); cout << "cumpliendo asi su mision de transmitir el conocimiento�al�mundo y de aprender.";

	asignarColor(12);
	Console::SetCursorPosition(40, 28); cout << "- PRESIONA 'ENTER' PARA VOLVER AL MENU -";
	
}
void ventanaInstrucciones() {

	int tecla = 0;

	Console::Clear();
	imprimirMarco();

	//Imprimir por primera vez
	InstruccionesPaginaUno();

	//Bucle de impresiones manejado por keyboardHandle
	for (;;) {

		keyboardHandle(tecla);

		if (tecla == 2) { InstruccionesPaginaUno(); }
		else if (tecla == 4) { InstruccionesPaginaDos(); }
		else if (tecla == 5) { break; }

	}

}
void InstruccionesPaginaUno() {

	Console::Clear();
	imprimirMarco();

	asignarColor(12);
	Console::SetCursorPosition(47, 1); cout << "I N S T R U C C I O N E S";

	asignarColor(9);
	Console::SetCursorPosition(7, 3); cout << "Mision: ";
	asignarColor(10);
	Console::SetCursorPosition(7, 4); cout << "Guia al jugador a traves de distintos niveles adquiriendo conocimiento de los 'Sabios Ancestrales' para vencer ";
	Console::SetCursorPosition(7, 5); cout << "a los 'Ladrones del Saber' resolviendo sus acertijos matematicos y llegar a las 'Ciudades del Conocimiento'.";
	
	asignarColor(9);
	Console::SetCursorPosition(7, 7); cout << "Controles:";
	asignarColor(10);
	Console::SetCursorPosition(7, 8); cout << "- Utiliza las teclas 'W', 'A', 'S' y 'D' o flechas direccionales para mover al personaje.";
	
	Console::SetCursorPosition(7, 10); cout << "- Para resolver los ejercicios matematicos, elige una respuesta moviendo el";
	Console::SetCursorPosition(7, 11); cout << "  elector de alternativa con las teclas 'A' o 'D' o flechas direccionales.";

	asignarColor(9);
	Console::SetCursorPosition(7, 13); cout << "Mecanica del juego:";
	asignarColor(10);
	Console::SetCursorPosition(7, 14); cout << "1. Niveles: El juego consta de 3 mundos diferentes, cada uno con una dificultad creciente";

	Console::SetCursorPosition(7, 16); cout << "2. Vidas: El jugador comienza con 5 vidas en total. Se pierden vidas cuando: el tiempo restante";
	Console::SetCursorPosition(7, 17); cout << "   llega a 0, se interactua con los enemigos sin haber recibido el conocimiento de los sabios o";
	Console::SetCursorPosition(7, 18); cout << "   cuando el jugador responde incorrectamente las preguntas de los 'Ladrones del Saber'. ";
	
	Console::SetCursorPosition(7, 19); cout << "3. Tutores: En cada nivel, el jugador encontrara tres 'Sabios Ancestrales' por mundo, que le compartiran";
	Console::SetCursorPosition(7, 20); cout << "   conocimientos de suma, resta y ecuaciones, para interactuar con ellos debes colisionarlos.";

	Console::SetCursorPosition(7, 22); cout << "4. Enemigos: En cada nivel, el jugador encontrara tres 'Ladrones del Saber' por mundo, que intentaran";
	Console::SetCursorPosition(7, 23); cout << "   detener su avance. Para superarlos, el jugador debe resolver un acertijo matematico.";
	Console::SetCursorPosition(7, 24); cout << "   Si el jugador no logra resolverlo, perdera una vida y volvera al inicio del mundo";
	Console::SetCursorPosition(7, 25); cout << "5. Ejercicios matematicos: Los ejercicios matematicos se ponen mas complejos en cada mundo. Los temas";
	Console::SetCursorPosition(7, 26); cout << "   abordados son: sumas, restas y ecuaciones con una incognita.";

	asignarColor(12);
	Console::SetCursorPosition(112, 20); cout << "#";
	Console::SetCursorPosition(112, 21); cout << "###";
	Console::SetCursorPosition(112, 22); cout << "#####";
	Console::SetCursorPosition(112, 23); cout << "###";
	Console::SetCursorPosition(112, 24); cout << "#";
	Console::SetCursorPosition(13, 28); cout << "- PRESIONA 'ENTER' PARA VOLVER AL MENU     -     PRESIONA 'D' PARA IR A LA SIGUIENTE PAGINA -";
}
void InstruccionesPaginaDos() {

	Console::Clear();
	imprimirMarco();

	asignarColor(12);
	Console::SetCursorPosition(47, 1); cout << "I N S T R U C C I O N E S";

	asignarColor(10);
	Console::SetCursorPosition(7, 3); cout << "6. Objetivo: El jugador debe de llegar a las 'Ciudades del Conocimiento' de cada mundo. Cuando este";
	Console::SetCursorPosition(7, 4); cout << "   llega alli, el jugador es enviado al siguiente mundo.";

	Console::SetCursorPosition(7, 6); cout << "7. Puntaje: ";
	Console::SetCursorPosition(7, 7); cout << "   El puntaje incrementa al vencer ladrones del conocimiento. El tiempo que quede restante cuando el jugador";
	Console::SetCursorPosition(7, 8); cout <<"   completa el mundo sera sumado a su puntaje. Se otorga un puntaje adicional al interactuar con un sabio.";

	Console::SetCursorPosition(7, 10); cout <<"8: Portales: ";
	Console::SetCursorPosition(7, 11); cout <<"   En cada mundo hay 2 portales, al cruzar uno el jugador aparecera en el otro portal, ambos portales";
	Console::SetCursorPosition(7, 12); cout <<"   se pueden cruzar de forma ilimitada.";
	
	Console::SetCursorPosition(7, 14); cout <<"9. Mundos: Cada mundo tiene barreras que no se pueden cruzar, si intentas atravesarlas solo colisionaras ";
	Console::SetCursorPosition(7, 15); cout <<"   con ellas. Ademas, cada mundo empieza con un tiempo restante de 500, si este llega a 0, el jugador";
	Console::SetCursorPosition(7, 16); cout << "  pierde una vida. Si logra finalizar el juego, cada vida que le quede se multiplicara por 200";
	Console::SetCursorPosition(7, 17); cout << "  y se sumara a su puntaje final";

	asignarColor(12);
	Console::SetCursorPosition(3, 20); cout << "    #";
	Console::SetCursorPosition(3, 21); cout << "  ###";
	Console::SetCursorPosition(3, 22); cout << "#####";
	Console::SetCursorPosition(3, 23); cout << "  ###";
	Console::SetCursorPosition(3, 24); cout << "    #";
	Console::SetCursorPosition(13, 28); cout << "- PRESIONA 'ENTER' PARA VOLVER AL MENU     -     PRESIONA 'A' PARA IR A LA PAGINA ANTERIOR -";
}
void ventanaInteractiva(int& opcion_actual) {

	//Limpiando el mensaje anterior
	Console::Clear();

	imprimirMarco();
	mensajeInicio();

	//Imprimiendo por primera vez las opciones
	Console::SetCursorPosition(50, 22); cout << " NUEVA PARTIDA";
	Console::SetCursorPosition(50, 23); cout << " INSTRUCCIONES";
	Console::SetCursorPosition(50, 24); cout << " HISTORIA";
	Console::SetCursorPosition(50, 25); cout << " RANKING";
	Console::SetCursorPosition(50, 26); cout << " CREADORES";
	Console::SetCursorPosition(50, 27); cout << " SALIR";

	int posicion_vertical_elector = 0;
	int tecla = 0;

	//Reiniciando la opcion_actual
	opcion_actual = 1;

	for (;;) {

		//Imprimiendo por el icono elector
		Console::SetCursorPosition(48, 22 + posicion_vertical_elector); cout << O;
		_sleep(10);
		Console::SetCursorPosition(48, 22 + posicion_vertical_elector); cout << ' ';

		keyboardHandle(tecla);

		if (tecla == 1 && opcion_actual != 1) {
			--posicion_vertical_elector;
			--opcion_actual;
		}
		else if (tecla == 3 && opcion_actual != 6) {
			++posicion_vertical_elector;
			++opcion_actual;
		}
		else if (tecla == 5) {
			break;
		}
	}

}
void ventanaNuevaPartida(string* NombresDeUsuarios, int& partida_actual) {

	//Limpiando el mensaje anterior
	Console::Clear();

	//Ingrese un nombre de usuario entre 1 y 9 caracteres:
	string Usuario;

	do {
		for (int i = 0; i < LONGITUD; ++i) { Console::SetCursorPosition(i, 11); cout << (char)219; }
		for (int i = 0; i < LONGITUD; ++i) { Console::SetCursorPosition(i, 16); cout << (char)219; }
		Console::SetCursorPosition(32, 13);
		cout << "Ingrese un nombre de usuario entre 1 y 9 caracteres: " << endl;
		Console::SetCursorPosition(32, 14);
		getline(cin, Usuario);		
		Console::Clear();
	} while (Usuario.empty() || Usuario.length() > 9 || Usuario.length() < 1);

	NombresDeUsuarios[partida_actual] = Usuario;
	++partida_actual;

}
void OrdenarRanking(string* NombresDeUsuarios, int* PuntajesDeUsuarios) {

	int temp_puntaje;
	string temp_nombre;

	for (int i = 0; i < 19; ++i) {
		for (int j = i + 1; j < 20; ++j) {

			if (PuntajesDeUsuarios[i] < PuntajesDeUsuarios[j]) {
				temp_puntaje = PuntajesDeUsuarios[i];
				PuntajesDeUsuarios[i] = PuntajesDeUsuarios[j];
				PuntajesDeUsuarios[j] = temp_puntaje;

				temp_nombre = NombresDeUsuarios[i];
				NombresDeUsuarios[i] = NombresDeUsuarios[j];
				NombresDeUsuarios[j] = temp_nombre;
			}

		}
	}

}
void ventanaRankings(string* NombresDeUsuarios, int* PuntajesDeUsuarios) {

	//Limpiando el mensaje anterior
	Console::Clear();
	imprimirMarco();

	int tecla = 0;

	Console::SetCursorPosition(52, 3); cout << "- R A N K I N G -";

	OrdenarRanking(NombresDeUsuarios, PuntajesDeUsuarios);

	for (int i = 0; i < 20; ++i) {

		Console::SetCursorPosition(40, 5 + i);
		cout << i + 1 << ".";

		Console::SetCursorPosition(44, 5 + i);
		cout << NombresDeUsuarios[i];

		Console::SetCursorPosition(74, 5 + i);
		cout << PuntajesDeUsuarios[i];
	}

	Console::SetCursorPosition(44, 28); cout << "- PRESIONA ENTER PARA SALIR -";

	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}

}
void ventanaCreadores() {

	//Limpiando el mensaje anterior
	Console::Clear();
	imprimirMarco();

	int tecla = 0;

	Console::SetCursorPosition(47, 3); cout << "- C R E A D O R E S -";
	Console::SetCursorPosition(35, 10); cout << O << " Leonardo Rafael Garcia Villanueva | U20231H059";
	Console::SetCursorPosition(35, 13); cout << O << " Adriano Samir Centeno Leon | U20241D920";
	Console::SetCursorPosition(35, 16); cout << O << " Julio Frank Quispe Serrano | U20241D922";
	Console::SetCursorPosition(44, 28); cout << "- PRESIONA ENTER PARA SALIR -";

	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}

}   
void ventanaSalir(bool& salida) {

	//Limpiando el mensaje anterior
	Console::Clear();
	imprimirMarco();

	int tecla = 0;

	int opcion_actual_local = 1;
	int posicion_horizontal_elector = 0;

	Console::SetCursorPosition(43, 5); cout << "ESTAS SEGURO DE QUE QUIERES SALIR?";
	Console::SetCursorPosition(52, 8); cout << "SI";
	Console::SetCursorPosition(64, 8); cout << "NO";

	for (;;) {

		keyboardHandle(tecla);

		//Imprimiendo por el elector
		Console::SetCursorPosition(50 + posicion_horizontal_elector * 12, 8); cout << O;
		_sleep(10);
		Console::SetCursorPosition(50 + posicion_horizontal_elector * 12, 8); cout << ' ';

		if (tecla == 2 && opcion_actual_local != 1) {
			--posicion_horizontal_elector;
			--opcion_actual_local;
		}
		else if (tecla == 4 && opcion_actual_local != 2) {
			++posicion_horizontal_elector;
			++opcion_actual_local;
		}
		else if (tecla == 5) {
			break;
		}
	}

	switch (opcion_actual_local) {
	case 1: salida = true;
	case 2: break;
	}

}

//Juego
void PartidaEnJuego(int& PuntajesDeUsuarios, string NombresDeUsuarios) {

	Partida_t Partida;
	Partida.vidas_restantes = 5;
	Partida.mundo_superado = new bool[3];
	for (int i = 0; i < 3; ++i) {
		Partida.mundo_superado[i] = false;
	}
	Partida.progreso_aprendizaje = 0;

	Console::Clear();
	MundoUno(Partida, PuntajesDeUsuarios, NombresDeUsuarios);

	if (Partida.mundo_superado[0] == true) {
		MundoDos(Partida, PuntajesDeUsuarios, NombresDeUsuarios);
	}
	if (Partida.mundo_superado[1] == true) {
		MundoTres(Partida, PuntajesDeUsuarios, NombresDeUsuarios);
	}

	if (Partida.vidas_restantes <= 0) {
		ventanaPerdiste(PuntajesDeUsuarios);
	}
	else {
		PuntajesDeUsuarios += Partida.vidas_restantes * 200;
		ventanaGanaste(PuntajesDeUsuarios);
	}
	delete[]Partida.mundo_superado;
}

//Mundos
void PintarMundo(short Mundo[24][120], int y, int x) {
	switch (Mundo[y][x]) {
	case 0:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Green;
		break;
	case 1:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Cyan;
		break;
	case 2:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::DarkGreen;
		break;
	case 3:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::DarkYellow;
		break;
	case 4:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Yellow;
		break;
	case 5:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Blue;
		break;
	case 6:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::DarkGray;
		break;
	case 7:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Gray;
		break;
	case 8:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::DarkCyan;
		break;
	case 9:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Black;
		break;
	case 10:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::White;
		break;
	case 11:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Magenta;
		break;
	case 12:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::DarkMagenta;
		break;
	case 13:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::Red;
		break;
	case 14:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::DarkRed;
		break;
	case 15:
		Console::SetCursorPosition(x, y);
		Console::BackgroundColor = ConsoleColor::DarkBlue;
		break;
	}
}
void ImprimirMundo(short Mundo[24][120]) {

	Console::Clear();
	for (int i = 0; i < ALTITUD - 6; ++i) {
		for (int j = 0; j < LONGITUD; ++j) {
			PintarMundo(Mundo, i, j);
			cout << " ";
		}
	}

}
void MundoUno(Partida_t& Partida, int& PuntajesDeUsuarios, string NombresDeUsuarios) {

	// Mapa
	short Mundo1[24][120] = { {2,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2},
							  {2,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2},
							  {2,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2},
							  {2,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0},
							  {2,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,0,0,0,0,0,2,2,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,2,2,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {2,0,0,0,0,0,2,2,2,2,2,1,1,1,1,2,2,2,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,0,3,3,3,0,0,3,3,3,0,4,4,4,0,3,3,3,0,0,3,3,3,0},
							  {2,0,0,0,0,0,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,2,2,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,0,3,3,3,3,3,3,3,3,3,4,4,4,3,3,3,3,3,3,3,3,3,0},
							  {2,0,0,0,0,0,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,0,0,0,0,0,2,2,2,2,2,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3},
							  {2,0,0,0,0,0,2,2,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,0,2,2,3,3,3,4,4,0,0,0,2,0,4,4,4,0,0,0,0,0,4,4,3,3,3},
							  {2,0,0,0,0,0,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,3,3,4,4,0,0,2,0,0,0,4,4,4,0,2,0,0,0,0,4,4,3,3},
							  {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,2,2,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,3,3,4,0,0,0,0,0,0,0,4,4,4,0,0,0,2,0,0,0,4,3,3},
							  {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,2,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,3,3,4,0,2,0,0,2,0,0,4,4,4,0,0,0,0,0,0,0,4,3,3},
							  {2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,2,1,1,1,1,1,1,1,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,0,0,0,0,2,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,2,2,3,3,4,0,0,0,0,0,0,0,4,4,4,0,2,0,0,0,2,0,4,3,3} };

	// Jugador
	Jugador_t Jugador;
	Jugador.pos_x = 2;
	Jugador.pos_y = 1;
	Jugador.direccion = 0;
	Jugador.items_recibidos = new bool[3];
	for (int i = 0; i < 3; ++i) {
		Jugador.items_recibidos[i] = false;
	}

	// Tutores
	Tutor_t Tutor;
	Tutor.pos_x[0] = 2; Tutor.pos_x[1] = 58; Tutor.pos_x[2] = 65;
	Tutor.pos_y[0] = 20; Tutor.pos_y[1] = 14; Tutor.pos_y[2] = 4;
	Tutor.interaccion = new short[3];
	for (int i = 0; i < 3; ++i) {
		Tutor.interaccion[i] = 0;
	}

	// Enemigos
	Enemigo_t Enemigo;
	Enemigo.pos_x[0] = 57; Enemigo.pos_x[1] = 44; Enemigo.pos_x[2] = 98;
	Enemigo.pos_y[0] = 9; Enemigo.pos_y[1] = 20; Enemigo.pos_y[2] = 5;
	Enemigo.mov_x = new int[3];
	Enemigo.mov_y = new int[3];
	Enemigo.mov_max = new bool[3];
	Enemigo.interaccion = new short[3];
	Enemigo.vencido = new short[3];

	for (int i = 0; i < 3; ++i) {
		Enemigo.mov_x[i] = 0;
		Enemigo.mov_y[i] = 0;
		Enemigo.mov_max[i] = false;
		Enemigo.interaccion[i] = 0;
		Enemigo.vencido[i] = 0;
	}

	// Portales
	Portal_t Portal;
	Portal.pos_x[0] = 11; Portal.pos_x[1] = 64;
	Portal.pos_y[0] = 11; Portal.pos_y[1] = 20;

	// Meta
	Meta_t Meta;
	Meta.pos_x = 106;
	Meta.pos_y = 16;

	// Datos de la Scoreboard del mundo
	Scoreboard_t Scoreboard;
	Scoreboard.imprimir = true;
	Scoreboard.mundo = 1;
	Scoreboard.tiempo = 500;
	int tiempo_retardado = 0;
	
	ImprimirMundo(Mundo1);
	
	do {
		if (Scoreboard.tiempo == 0) { 
			--Partida.vidas_restantes; 
			Scoreboard.tiempo = 800; 
		}
		LimpiarEnemigo(Enemigo, Mundo1);
		MovimientoEnemigo_Mundo1(Enemigo);
		MovimientoJugador(Jugador, Tutor, Enemigo, Meta, Mundo1, Partida.mundo_superado[0], Scoreboard.mundo);
		ImprimirJugador(Jugador, Mundo1);
		ImprimirEnemigo(Enemigo, Mundo1);
		ImprimirTutor(Tutor, Mundo1);
		ImprimirPortal(Portal, Mundo1);
		InteraccionTutores_Mundo1(Jugador, Tutor, Partida.progreso_aprendizaje, PuntajesDeUsuarios);
		InteraccionEnemigos_Mundo1(Jugador, Enemigo, Partida, PuntajesDeUsuarios, Mundo1);
		FuncionAdicionalPortal(Jugador, Portal, Mundo1, Scoreboard.mundo);
		ScoreBoard(Partida, Scoreboard, PuntajesDeUsuarios, NombresDeUsuarios);
		_sleep(20);
		++tiempo_retardado;
		if (tiempo_retardado == 5) {
			--Scoreboard.tiempo;
			tiempo_retardado = 0;
		}

	} while (Partida.mundo_superado[0] == false && Partida.vidas_restantes > 0);

	PuntajesDeUsuarios += Scoreboard.tiempo;

	delete[]Jugador.items_recibidos;
	delete[]Tutor.interaccion;
	delete[]Enemigo.mov_x; 
	delete[]Enemigo.mov_y; 
	delete[]Enemigo.mov_max; 
	delete[]Enemigo.interaccion;
	delete[]Enemigo.vencido;

}
void MundoDos(Partida_t& Partida, int& PuntajesDeUsuarios, string NombresDeUsuarios) {

	// Mapa
	short Mundo2[24][120] = { {3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,4,4,7,4,4,4,4,4,4,4,4,4,7,7,7,7,7,7,7,7,7,7,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,3,3,3,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,3,3,3,4,4,4,4,4,4,3,3,3,3,3,3,4,4,4,4,4,3,3,3,3,3,3,4,4,4,4,4,3,3,3,3,3,3,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,7,4,4,4,7,7,4,4,4,4,7,7,7,7,7,7,7,4,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,4,4,4,5,5,5,7,7,7,7,7,7,7,7,7,7,5,5,3,3,3,4,3,3,3,3,3,4,4,4,4,3,3,3,4,3,3,3,4,4,4,4,3,3,3,4,3,3,3,4,4,4,4,3,3,3,4,4,4,4,4,3,4,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,7,7,4,4,4,7,7,4,4,7,7,7,7,7,7,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,4,4,5,5,5,7,7,2,0,0,0,0,2,0,0,7,7,5,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,3,4,4,4,4,4,4,4,3,3,3,4,4,3,3,3,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,4,7,7,4,4,4,7,7,7,7,7,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,4,4,4,5,7,7,0,0,0,2,0,2,0,0,2,0,7,5,3,3,3,3,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,3,3,3,3,3,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,7,7,7,7,7,7,7,7,7,7,7,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,5,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,4,7,7,7,7,7,4,4,4,4,7,7,4,4,4,4,4,4,3,3,3,3,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,7,5,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,4,4,4,7,7,7,7,7,7,4,4,4,4,4,7,4,4,4,3,3,3,3,3,3,3,3,3,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,4,4,4,5,7,7,0,0,2,0,0,0,2,0,0,0,7,5,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,4,3,3,3,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,7,7,7,7,4,4,4,4,7,7,7,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,3,3,3},
							  {3,3,3,4,4,4,4,4,4,4,4,4,4,5,5,5,7,7,0,0,0,0,2,0,0,2,7,7,5,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,7,7,7,7,7,7,4,4,4,4,4,4,4,7,4,4,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,4,4,4,5,5,5,7,7,7,7,7,7,7,7,7,7,5,5,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,4,4,7,7,7,7,7,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,3,3,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,7,7,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,3,4,4,4,4,4,4,3,4,4,3,3,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,4,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,3,3,3,3,3,3,4,4,4,4,3,3,4,4,4,4,4,4,4,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,7,4,4,4,7,4,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,3,3,3,3,4,4,4,4,3,3,3,3,3,4,4,4,4,4,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4,3,4,4,4,4,4,4,3,4,4,4,3,3,4,4,4,4,4,4,4,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,7,7,7,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,3,3,4,3,4,3,3,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,3,4,4,3,3,4,4,3,4,4,3,3,4,4,4,4,4,4,4,4,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,4,3,4,4,4,7,4,4,4,7,4,4,4,7,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,3,3,3,4,4,4,4,4,3,3,4,4,4,4,4,3,3,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,3,3,4,4,3,4,4,4,3,3,4,4,4,3,4,3,3,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,4,4,4,7,7,7,7,7,7,7,4,4,4,4,4,4,3,3,4,4,4,8,8,8,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,3,3,3,3,4,4,3,3,4,4,4,3,4,4,4,3,3,4,4,4,3,3,3,3,4,4,4,4,4,4,4,3,3,4,4,4,3,3,4,4,4,4,4,4,3,3,4,4,3,3,4,4,4,4,4,4,4,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,7,4,4,4,4,4,4,4,4,3,3,4,4,4,4,8,8,8,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,3,4,4,4,4,3,4,4,4,4,3,3,4,4,4,4,4,4,4,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,7,4,4,4,4,4,4,4,4,3,3,4,4,4,4,8,8,8,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,3,3,3,3,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,4,4,4,4,3,4,4,4,4,3,4,4,4,3,3,4,4,4,4,4,4,4,4,4,3,4,3,4,3,4,4,4,4,4,4,4,4,4,4,3,4,4,4,4,7,7,7,7,7,7,7,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,3,4,4,3,4,4,4,4,3,3,4,4,4,4,4,4,4,4,3,3,3,3,3,3,3,4,4,4,4,4,4,4,4,3,3,3,4,4,7,4,4,7,7,7,4,4,7,4,4,4,4,4,3,3,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,3,4,4,3,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,7,7,7,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,3,3,4,4,4,4,4,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,7,4,4,4,4,4,4,4,3,3,3,3,3,4,5,5,5,4,4,4,4,4,4,4,4,4,4,4,4},
							  {4,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,4,4,4,4,3,3,4,4,3,3,4,4,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,4,4,5,5,5,4,4,4,4,4,4,4,4,4,4,4,4},
							  {3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,4,3,3,3,4,4,4,4,4,4,3,3,4,3,3,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,4,4,4,4,4,4,4,4,4,4,4,4,3,3,3,3,3,4,4,4,5,5,5,4,4,4,4,4,4,4,4,4,4,4,4} };

	// Jugador
	Jugador_t Jugador;
	Jugador.pos_x = 105;
	Jugador.pos_y = 20;
	Jugador.direccion = 0;
	Jugador.items_recibidos = new bool[3];
	for (int i = 0; i < 3; ++i) {
		Jugador.items_recibidos[i] = false;
	}

	// Tutores
	Tutor_t Tutor;
	Tutor.pos_x[0] = 106; Tutor.pos_x[1] = 64; Tutor.pos_x[2] = 38;
	Tutor.pos_y[0] = 14; Tutor.pos_y[1] = 14; Tutor.pos_y[2] = 14;
	Tutor.interaccion = new short[3];
	for (int i = 0; i < 3; ++i) {
		Tutor.interaccion[i] = 0;
	}

	// Enemigos
	Enemigo_t Enemigo;
	Enemigo.pos_x[0] = 71; Enemigo.pos_x[1] = 50; Enemigo.pos_x[2] = 22;
	Enemigo.pos_y[0] = 20; Enemigo.pos_y[1] = 6; Enemigo.pos_y[2] = 20;
	Enemigo.mov_x = new int[3];
	Enemigo.mov_y = new int[3];
	Enemigo.mov_max = new bool[3];
	Enemigo.interaccion = new short[3];
	Enemigo.vencido = new short[3];

	for (int i = 0; i < 3; ++i) {
		Enemigo.mov_x[i] = 0;
		Enemigo.mov_y[i] = 0;
		Enemigo.mov_max[i] = false;
		Enemigo.interaccion[i] = 0;
		Enemigo.vencido[i] = 0;
	}

	// Portales
	Portal_t Portal;
	Portal.pos_x[0] = 109; Portal.pos_x[1] = 76;
	Portal.pos_y[0] = 9; Portal.pos_y[1] = 0;

	// Meta
	Meta_t Meta;
	Meta.pos_x = 11;
	Meta.pos_y = 6;

	// Datos de la Scoreboard del mundo
	Scoreboard_t Scoreboard;
	Scoreboard.imprimir = true;
	Scoreboard.mundo = 2;
	Scoreboard.tiempo = 500;
	int tiempo_retardado = 0;

	ImprimirMundo(Mundo2);

	do {
		if (Scoreboard.tiempo == 0) {
			--Partida.vidas_restantes;
			Scoreboard.tiempo = 800;
		}
		LimpiarEnemigo(Enemigo, Mundo2);
		MovimientoEnemigo_Mundo2(Enemigo);
		MovimientoJugador(Jugador, Tutor, Enemigo, Meta, Mundo2, Partida.mundo_superado[1], Scoreboard.mundo);
		ImprimirJugador(Jugador, Mundo2);
		ImprimirEnemigo(Enemigo, Mundo2);
		ImprimirTutor(Tutor, Mundo2);
		ImprimirPortal(Portal, Mundo2);
		InteraccionTutores_Mundo2(Jugador, Tutor, Partida.progreso_aprendizaje, PuntajesDeUsuarios);
		InteraccionEnemigos_Mundo2(Jugador, Enemigo, Partida, PuntajesDeUsuarios, Mundo2);
		FuncionAdicionalPortal(Jugador, Portal, Mundo2, Scoreboard.mundo);
		ScoreBoard(Partida, Scoreboard, PuntajesDeUsuarios, NombresDeUsuarios);
		_sleep(20);
		++tiempo_retardado;
		if (tiempo_retardado == 5) {
			--Scoreboard.tiempo;
			tiempo_retardado = 0;
		}

	} while (Partida.mundo_superado[1] == false && Partida.vidas_restantes > 0);

	PuntajesDeUsuarios += Scoreboard.tiempo;

	delete[]Jugador.items_recibidos;
	delete[]Tutor.interaccion;
	delete[]Enemigo.mov_x;
	delete[]Enemigo.mov_y;
	delete[]Enemigo.mov_max;
	delete[]Enemigo.interaccion;
	delete[]Enemigo.vencido;
}
void MundoTres(Partida_t& Partida, int& PuntajesDeUsuarios, string NombresDeUsuarios) {

	// Mapa
	short Mundo3[24][120] = { {10,10,10,10,10,10,10,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,7,7,7,10,10,10,10,10,10,7,7,7,10,10,10,10,10,10,7,7,7,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0},
							  {10,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,0},
							  {10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,10,10,10,7,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,10,10,7,10,10,7,7,0,0,0,0,0,0,0,0,0,0},
							  {10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,10,10,7,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,7,10,10,7,7,0,0,0,0,0,0,0,0,0},
							  {10,10,10,7,7,0,0,0,0,0,0,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,7,7,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,7,7,7,7,7,7,0,0,0,0},
							  {10,10,7,7,0,0,0,0,0,0,0,0,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,7,7,10,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,7,7,3,0,3,0,6,6,0,3,0,3,0,0,0,0,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,7,7,7,7,7,7,10,10,10,7,0,0,0},
							  {10,10,7,0,0,0,0,0,0,0,0,0,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,10,10,10,10,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,7,7,3,3,3,3,3,6,6,3,3,3,3,3,0,0,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,10,10,10,10,7,7,7,10,7,0,0,0},
							  {10,7,7,0,0,0,0,0,0,0,0,0,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,10,10,10,10,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,7,3,3,4,4,4,4,6,6,4,4,4,4,3,3,7,7,0,0,0,0,0,0,0,0,0,0,7,7,7,7,7,7,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,10,10,10,7,7,0,0,0},
							  {7,7,0,0,0,0,0,0,0,0,0,0,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,10,10,7,7,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,7,3,4,4,4,4,4,6,6,4,4,4,4,4,3,7,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,7,7,0,0,0,0,0,0,0,0,7,10,10,10,7,7,10,10,10,10,7,7,0,0,0},
							  {7,0,0,0,0,0,0,0,0,0,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,0,7,7,7,7,10,10,10,10,10,7,7,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,7,3,3,4,4,4,4,4,4,4,4,4,4,3,3,7,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,7,0,0,0,0,0,0,0,0,7,7,10,10,7,7,10,10,10,7,7,7,0,0,0},
							  {7,0,0,0,0,0,0,0,0,7,7,7,10,10,7,10,7,0,0,0,0,0,0,0,0,7,7,7,10,10,10,10,10,10,10,10,10,7,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,7,3,3,3,3,3,3,3,3,3,3,3,3,3,3,7,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,10,7,0,0,0,0,0},
							  {0,0,0,0,0,0,0,0,0,7,7,10,10,10,7,7,7,7,7,7,7,7,7,7,7,7,10,10,10,10,10,10,10,10,10,10,10,7,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,7,7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,0,7,10,10,10,7,7,10,10,10,7,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,7,7,0,0,0,0,0},
							  {0,0,0,0,0,0,0,0,0,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,7,10,10,10,7,7,10,10,10,7,0,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,7,0,0,0,0,0,0},
							  {0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,10,7,7,7,7,10,10,10,10,7,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,7,7,7,10,10,10,10,10,7,0,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,7,7,0,0,0,0,0,0},
							  {0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,7,10,10,10,10,7,7,7,10,10,10,10,10,7,0,0,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,7,0,0,0,0,0,0,0,0,0,0,0,0,7,10,10,10,7,0,0,0,0,0,0,0},
							  {0,0,0,0,0,0,0,0,0,7,10,10,7,7,7,7,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,7,7,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,10,10,10,10,10,7,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,7,7,0,0,0,0,0,0},
							  {0,0,0,0,0,0,0,0,0,7,7,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,7,7,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,10,7,7,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,7,0,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,7,0,0,0,0,0,0},
							  {7,7,7,7,7,7,7,7,7,7,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,10,10,10,7,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,10,10,10,10,10,7,0,0,0,0,0,0,0,0,0,7,7,10,10,7,10,10,7,7,0,0,0,0,0},
							  {10,10,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,7,7,7,7,10,10,10,10,7,10,10,10,10,10,10,10,10,10,10,10,7,10,10,10,10,7,1,1,1,1,1,1,1,1,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,10,7,7,10,7,7,0,0,0,0,0,0,0,0,0,7,10,10,10,10,10,10,10,7,0,0,0,0,0},
							  {10,10,10,10,10,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,7,7,7,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,7,7,7,7,0,0,0,0,0,0,0,0,0,7,7,7,7,7,7,7,7,7,0,0,0,0,0},
							  {10,10,10,10,10,10,10,10,10,10,10,10,7,7,7,10,10,10,10,10,10,10,7,7,7,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,10,10,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,7,10,10,10,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {10,10,10,10,10,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,7,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,1,1,1,1,1,1,1,1,1,1,7,10,10,7,7,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,7,7,7,7,7,10,10,10,10,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {10,10,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,10,7,1,1,1,1,1,1,1,1,7,10,10,10,10,10,10,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,7,10,10,10,10,10,10,7,7,10,10,10,7,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},
							  {10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,10,10,10,10,7,1,1,1,1,1,1,1,1,7,10,10,10,10,10,7,7,7,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,7,7,7,10,10,10,10,10,10,7,7,10,10,10,10,7,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0} };

	// Jugador
	Jugador_t Jugador;
	Jugador.pos_x = 9;
	Jugador.pos_y = 5;
	Jugador.direccion = 0;
	Jugador.items_recibidos = new bool[3];
	for (int i = 0; i < 3; ++i) {
		Jugador.items_recibidos[i] = false;
	}

	// Tutores
	Tutor_t Tutor;
	Tutor.pos_x[0] = 20; Tutor.pos_x[1] = 101; Tutor.pos_x[2] = 81;
	Tutor.pos_y[0] = 8; Tutor.pos_y[1] = 12; Tutor.pos_y[2] = 12;
	Tutor.interaccion = new short[3];
	for (int i = 0; i < 3; ++i) {
		Tutor.interaccion[i] = 0;
	}

	// Enemigos
	Enemigo_t Enemigo;
	Enemigo.pos_x[0] = 111; Enemigo.pos_x[1] = 94; Enemigo.pos_x[2] = 66;
	Enemigo.pos_y[0] = 20; Enemigo.pos_y[1] = 3; Enemigo.pos_y[2] = 18;
	Enemigo.mov_x = new int[3];
	Enemigo.mov_y = new int[3];
	Enemigo.mov_max = new bool[3];
	Enemigo.interaccion = new short[3];
	Enemigo.vencido = new short[3];

	for (int i = 0; i < 3; ++i) {
		Enemigo.mov_x[i] = 0;
		Enemigo.mov_y[i] = 0;
		Enemigo.mov_max[i] = false;
		Enemigo.interaccion[i] = 0;
		Enemigo.vencido[i] = 0;
	}

	// Portales
	Portal_t Portal;
	Portal.pos_x[0] = 3; Portal.pos_x[1] = 111;
	Portal.pos_y[0] = 13; Portal.pos_y[1] = 0;

	// Meta
	Meta_t Meta;
	Meta.pos_x = 69;
	Meta.pos_y = 5;

	// Datos de la Scoreboard del mundo
	Scoreboard_t Scoreboard;
	Scoreboard.imprimir = true;
	Scoreboard.mundo = 3;
	Scoreboard.tiempo = 500;
	int tiempo_retardado = 0;

	ImprimirMundo(Mundo3);

	do {
		if (Scoreboard.tiempo == 0) {
			--Partida.vidas_restantes;
			Scoreboard.tiempo = 800;
		}
		LimpiarEnemigo(Enemigo, Mundo3);
		MovimientoEnemigo_Mundo3(Enemigo);
		MovimientoJugador(Jugador, Tutor, Enemigo, Meta, Mundo3, Partida.mundo_superado[2], Scoreboard.mundo);
		ImprimirJugador(Jugador, Mundo3);
		ImprimirEnemigo(Enemigo, Mundo3);
		ImprimirTutor(Tutor, Mundo3);
		ImprimirPortal(Portal, Mundo3);
		InteraccionTutores_Mundo3(Jugador, Tutor, Partida.progreso_aprendizaje, PuntajesDeUsuarios);
		InteraccionEnemigos_Mundo3(Jugador, Enemigo, Partida, PuntajesDeUsuarios, Mundo3);
		FuncionAdicionalPortal(Jugador, Portal, Mundo3, Scoreboard.mundo);
		ScoreBoard(Partida, Scoreboard, PuntajesDeUsuarios, NombresDeUsuarios);
		_sleep(20);
		++tiempo_retardado;
		if (tiempo_retardado == 5) {
			--Scoreboard.tiempo;
			tiempo_retardado = 0;
		}

	} while (Partida.mundo_superado[2] == false && Partida.vidas_restantes > 0);

	PuntajesDeUsuarios += Scoreboard.tiempo;

	delete[]Jugador.items_recibidos;
	delete[]Tutor.interaccion;
	delete[]Enemigo.mov_x;
	delete[]Enemigo.mov_y;
	delete[]Enemigo.mov_max;
	delete[]Enemigo.interaccion;
	delete[]Enemigo.vencido;

}

//Mensajes de tutores y enemigos del mundo 1, ecuaciones de suma
void Msg_Tutor1_Mundo1() {

	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Blue;
	Console::SetCursorPosition(70, 26);  cout << "<--------- SUMA --------->";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "La suma es como juntar cosas. Imagina que tienes algunas manzanas";
	Console::SetCursorPosition(52, 28);  cout << "y luego consigues mas manzanas. La suma es el proceso de contar";
	Console::SetCursorPosition(52, 29);  cout << "cuantas manzanas tienes en total.";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
void Msg_Tutor2_Mundo1() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Cyan;
	Console::SetCursorPosition(52, 26);  cout << "Veo que sabes sumar numeros de una cifra... Te contare algo mas.";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Los numeros se agrupan en 'UNIDADES','DECENAS','CENTENAS', etc.";
	Console::SetCursorPosition(52, 28);  cout << "Cuando el total de la suma sea un numero mayor a 9 tendras que";
	Console::SetCursorPosition(52, 29);  cout << "empezar a acumular en la siguiente cifra.";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
void Msg_Tutor3_Mundo1() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Cyan;
	Console::SetCursorPosition(52, 26);  cout << "Asi que sabes sumar numeros de dos cifras... Pero sabes de tres?";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Cuando tengas que sumar numeros de 2 o mas cifras hazlo de";
	Console::SetCursorPosition(52, 28);  cout << "forma vertical, empezando por la derecha y yendo a la izquierda.";
	Console::SetCursorPosition(52, 29);  cout << "No te olvides de llevar cuando la suma sea mayor que 9.";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
bool Msg_Enemigo1_Mundo1(int var1, int var2, int respuesta) {

	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Dame tu conocimiento o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(65, 27); cout << var1 << "+";
	Console::SetCursorPosition(65, 28); cout << var2;
	asignarColor(10);
	Console::SetCursorPosition(67, 28); cout << "     A. " << respuesta << "     B. " << var1 << "     C. " << var2;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 9;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 88) {
				pos_x_elector += 9;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 70) {
		return true;
	}
	else {
		return false;
	}
}
bool Msg_Enemigo2_Mundo1(int var1, int var2, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Dame tu conocimiento o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(65, 27); cout << var1 << "+";
	Console::SetCursorPosition(65, 28); cout << var2;
	asignarColor(10);
	Console::SetCursorPosition(67, 28); cout << "     A. " << var2 << "     B. " << respuesta << "     C. " << var1;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 10;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 90) {
				pos_x_elector += 10;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 80) {
		return true;
	}
	else {
		return false;
	}
}
bool Msg_Enemigo3_Mundo1(int var1, int var2, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Dame tu conocimiento o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(65, 27); cout << var1 << "+";
	Console::SetCursorPosition(65, 28); cout << var2;
	asignarColor(10);
	Console::SetCursorPosition(68, 28); cout << "    A. " << respuesta << "     B. " << var1 << "     C. " << var2;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 11;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 92) {
				pos_x_elector += 11;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 70) {
		return true;
	}
	else {
		return false;
	}
}

//Mensajes de tutores y enemigos del mundo 2, ecuaciones de resta
void Msg_Tutor1_Mundo2() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Blue;
	Console::SetCursorPosition(77, 26);  cout << "<--------- RESTA --------->";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "La resta es como quitar cosas. Imagina que tienes algunas manzanas";
	Console::SetCursorPosition(52, 28);  cout << "y luego te comes algunas. La resta es el proceso de contar cuantas";
	Console::SetCursorPosition(52, 29);  cout << "manzanas te quedan.";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
void Msg_Tutor2_Mundo2() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Cyan;
	Console::SetCursorPosition(52, 26);  cout << "Excelente, veo que ya sabes restar numeros de una cifra, pero...";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Cuando queremos restar numeros de dos o mas digitos escribe los ";
	Console::SetCursorPosition(52, 28);  cout << "numeros uno debajo del otro, alineados por las columnas, los restas";
	Console::SetCursorPosition(52, 29);  cout << "de derecha a izquierda. Prestate una cifra si es necesario.";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
void Msg_Tutor3_Mundo2() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Cyan;
	Console::SetCursorPosition(52, 26);  cout << "Muy bien, ya sabes como restar numeros, pero te recuerdo:";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Cuando el digito que esta en la parte superior de la columna sea";
	Console::SetCursorPosition(52, 28);  cout << "menor que el de la parte inferior, tendras que prestarte de la";
	Console::SetCursorPosition(52, 29);  cout << "cifra a la izquierda, restandole 1, y sumando 1 al digito.";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
bool Msg_Enemigo1_Mundo2(int var1, int var2, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Dame tu conocimiento o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(65, 27); cout << var1 << "-";
	Console::SetCursorPosition(65, 28); cout << var2;
	asignarColor(10);
	Console::SetCursorPosition(67, 28); cout << "     A. " << var2 << "     B. " << var1 << "     C. " << respuesta;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 9;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 88) {
				pos_x_elector += 9;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 88) {
		return true;
	}
	else {
		return false;
	}
}
bool Msg_Enemigo2_Mundo2(int var1, int var2, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Dame tu conocimiento o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(65, 27); cout << var1 << "-";
	Console::SetCursorPosition(65, 28); cout << var2;
	asignarColor(10);
	Console::SetCursorPosition(67, 28); cout << "     A. " << setw(2) << respuesta << "     B. " << var2 << "     C. " << var1;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 10;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 90) {
				pos_x_elector += 10;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 70) {
		return true;
	}
	else {
		return false;
	}
}
bool Msg_Enemigo3_Mundo2(int var1, int var2, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Dame tu conocimiento o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(65, 27); cout << var1 << "-";
	Console::SetCursorPosition(65, 28); cout << var2;
	asignarColor(10);
	Console::SetCursorPosition(68, 28); cout << "    A. " << var1 << "     B. " << setw(3) << respuesta << "     C. " << var2;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 11;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 92) {
				pos_x_elector += 11;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 81) {
		return true;
	}
	else {
		return false;
	}
}

//Mensajes de tutores y enemigos del mundo 3, ecuaciones con una incognita
void Msg_Tutor1_Mundo3() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Blue;
	Console::SetCursorPosition(69, 26);  cout << "<--------- INCOGNITA --------->";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Una ecuacion es como un rompecabezas en matematicas donde necesitas ";
	Console::SetCursorPosition(52, 28);  cout << "encontrar el valor de una incognita. La incognita es algo que no ";
	Console::SetCursorPosition(52, 29);  cout << "sabemos y usualmente la representamos con una letra, como 'x'. ";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();

	Console::ForegroundColor = ConsoleColor::Blue;
	Console::SetCursorPosition(69, 26);  cout << "<--------- INCOGNITA --------->";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Aqui te dejo un ejemplo: x + 5 = 12";
	Console::SetCursorPosition(52, 28);  cout << "Primero debes de despejar la variable pasando el 5 al otro lado ";
	Console::SetCursorPosition(52, 29);  cout << "con el signo contrario: x = 12 - 5. Lo que resulta en: x = 7 ";

	tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();

}
void Msg_Tutor2_Mundo3() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Cyan;
	Console::SetCursorPosition(52, 26);  cout << "Asi que ya sabes algo sobre las ecuaciones con incognita...";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Recuerda despejar la variable y enviar los numeros que";
	Console::SetCursorPosition(52, 28);  cout << "la acompanan al otro lado con el signo cambiado, por ejemplo:";
	Console::SetCursorPosition(52, 29);  cout << "x - 52 = 32. Luego: x = 32 + 52. Lo que resulta en: x = 84";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
void Msg_Tutor3_Mundo3() {
	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Cyan;
	Console::SetCursorPosition(52, 26);  cout << "Veo que ya sabes sobre ecuaciones con una incognita...";
	Console::ForegroundColor = ConsoleColor::White;
	Console::SetCursorPosition(52, 27);  cout << "Te puedo dar un ejemplo para que no te olvides sobre ellas";
	Console::SetCursorPosition(52, 28);  cout << "x - 200 = 300 + 100. Luego x = 300 + 100 + 200.";
	Console::SetCursorPosition(52, 29);  cout << "Lo que resulta en: x = 600. Suerte en tu viaje!";

	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
	LimpiarMensajeScoreboard();
}
bool Msg_Enemigo1_Mundo3(int var1, int var2, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Dame tu conocimiento o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(61, 27); cout << "x + " << var1 << " = " << var2;
	asignarColor(10);
	Console::SetCursorPosition(67, 28); cout << "     A. " << respuesta << "     B. " << var1 << "     C. " << var2;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 9;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 88) {
				pos_x_elector += 9;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 70) {
		return true;
	}
	else {
		return false;
	}
}
bool Msg_Enemigo2_Mundo3(int var1, int var2, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Resuelve mi acertijo o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(61, 27); cout << "x - " << var1 << " = " << var2;
	asignarColor(10);
	Console::SetCursorPosition(67, 28); cout << "     A. " << var2 << "     B. " << setw(2) << respuesta << "     C. " << var1;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 10;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 90) {
				pos_x_elector += 10;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 80) {
		return true;
	}
	else {
		return false;
	}
}
bool Msg_Enemigo3_Mundo3(int var1, int var2, int var3, int respuesta) {
	int pos_x_elector = 70;

	asignarColor(9);
	Console::BackgroundColor = ConsoleColor::Black;
	Console::SetCursorPosition(60, 26); cout << "Alto ahi! Resuelve mi acertijo o no te dejare pasar!";
	asignarColor(11);
	Console::SetCursorPosition(61, 27); cout << "x - " << var3 << " = " << var2 << " + " << var1;
	asignarColor(10);
	Console::SetCursorPosition(68, 28); cout << "    A. " << var2 << "     B. " << var1 << "     C. " << setw(3) << respuesta;

	int tecla = 0;

	for (;;) {

		Console::SetCursorPosition(pos_x_elector, 28); cout << O;
		_sleep(10);
		Console::SetCursorPosition(pos_x_elector, 28); cout << ' ';

		keyboardHandle(tecla);
		if (tecla == 2) {
			if (pos_x_elector > 70) {
				pos_x_elector -= 11;
			}
		}
		else if (tecla == 4) {
			if (pos_x_elector < 92) {
				pos_x_elector += 11;
			}
		}
		else if (tecla == 5) {
			break;
		}
	}
	LimpiarMensajeScoreboard();

	if (pos_x_elector == 92) {
		return true;
	}
	else {
		return false;
	}
}

//Scoreboard
void ScoreBoard(Partida_t Partida, Scoreboard_t& Scoreboard, int PuntajesDeUsuarios, string NombresDeUsuarios) {

	Console::BackgroundColor = ConsoleColor::Black;
	Console::ForegroundColor = ConsoleColor::Gray;

	if (Scoreboard.imprimir == true) {
		for (int i = 0; i < 6; ++i) {
			for (int j = 0; j < LONGITUD; ++j) {
				Console::SetCursorPosition(j, i + 24); cout << " ";
			}
		}
		for (int i = 0; i < LONGITUD; ++i) { Console::SetCursorPosition(i, 24); cout << (char)219; }
		Scoreboard.imprimir = false;
	}

	Console::SetCursorPosition(2, 26); cout << "Vidas restantes: " << setw(4) << Partida.vidas_restantes;
	Console::SetCursorPosition(2, 27); cout << "Tiempo restante: " << setw(4) << Scoreboard.tiempo;
	Console::SetCursorPosition(2, 28); cout << "Progreso de aprendizaje: " << setw(3) << Partida.progreso_aprendizaje << "%";
	Console::SetCursorPosition(34, 26); cout << "Puntaje: " << setw(4) << PuntajesDeUsuarios;
	Console::SetCursorPosition(34, 27); cout << "Nombre: " << NombresDeUsuarios;
	Console::SetCursorPosition(34, 28); cout << "Mundo: " << Scoreboard.mundo;

}
void LimpiarMensajeScoreboard() {
	Console::SetCursorPosition(52, 25);  cout << "                                                                    ";
	Console::SetCursorPosition(52, 26);  cout << "                                                                    ";
	Console::SetCursorPosition(52, 27);  cout << "                                                                    ";
	Console::SetCursorPosition(52, 28);  cout << "                                                                    ";
	Console::SetCursorPosition(52, 29);  cout << "                                                                    ";
}

//Funciones del jugador
bool ColisionJugadorMapa(Jugador_t Jugador, short Mundo[24][120]) {

	// Mundo[y][x] == 2 (Mundo 1)
	// Mundo[y][x] == 3 (Mundo 2)
	// Mundo[y][x] == 7	(Mundo 3)

	switch (Jugador.direccion) {
	case 1:
		if (Jugador.pos_y < 0 || 
			Mundo[Jugador.pos_y][Jugador.pos_x] == 2 || Mundo[Jugador.pos_y][Jugador.pos_x + 1] == 2 || Mundo[Jugador.pos_y][Jugador.pos_x + 2] == 2 ||
			Mundo[Jugador.pos_y][Jugador.pos_x] == 3 || Mundo[Jugador.pos_y][Jugador.pos_x + 1] == 3 || Mundo[Jugador.pos_y][Jugador.pos_x + 2] == 3 ||
			Mundo[Jugador.pos_y][Jugador.pos_x] == 7 || Mundo[Jugador.pos_y][Jugador.pos_x + 1] == 7 || Mundo[Jugador.pos_y][Jugador.pos_x + 2] == 7) {
			return true;
		}
		break;
	case 2:
		if (Jugador.pos_x < 0 ||
			Mundo[Jugador.pos_y][Jugador.pos_x] == 2 || Mundo[Jugador.pos_y + 1][Jugador.pos_x] == 2 ||
			Mundo[Jugador.pos_y + 2][Jugador.pos_x] == 2 || Mundo[Jugador.pos_y + 3][Jugador.pos_x] == 2 ||
			Mundo[Jugador.pos_y][Jugador.pos_x] == 3 || Mundo[Jugador.pos_y + 1][Jugador.pos_x] == 3 ||
			Mundo[Jugador.pos_y + 2][Jugador.pos_x] == 3 || Mundo[Jugador.pos_y + 3][Jugador.pos_x] == 3 ||
			Mundo[Jugador.pos_y][Jugador.pos_x] == 7 || Mundo[Jugador.pos_y + 1][Jugador.pos_x] == 7 ||
			Mundo[Jugador.pos_y + 2][Jugador.pos_x] == 7 || Mundo[Jugador.pos_y + 3][Jugador.pos_x] == 7) {
			return true;
		}
		break;		
	case 3:
		if (Jugador.pos_y > ALTITUD - 1 - 9 ||
			Mundo[Jugador.pos_y + 3][Jugador.pos_x] == 2 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 1] == 2 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 2] == 2 ||
			Mundo[Jugador.pos_y + 3][Jugador.pos_x] == 3 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 1] == 3 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 2] == 3 ||
			Mundo[Jugador.pos_y + 3][Jugador.pos_x] == 7 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 1] == 7 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 2] == 7) {
			return true;
		}
		break;
	case 4:
		if (Jugador.pos_x + 2 > LONGITUD - 1 ||
			Mundo[Jugador.pos_y][Jugador.pos_x + 2] == 2 || Mundo[Jugador.pos_y + 1][Jugador.pos_x + 2] == 2 ||
			Mundo[Jugador.pos_y + 2][Jugador.pos_x + 2] == 2 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 2] == 2 ||
			Mundo[Jugador.pos_y][Jugador.pos_x + 2] == 3 || Mundo[Jugador.pos_y + 1][Jugador.pos_x + 2] == 3 ||
			Mundo[Jugador.pos_y + 2][Jugador.pos_x + 2] == 3 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 2] == 3 ||
			Mundo[Jugador.pos_y][Jugador.pos_x + 2] == 7 || Mundo[Jugador.pos_y + 1][Jugador.pos_x + 2] == 7 ||
			Mundo[Jugador.pos_y + 2][Jugador.pos_x + 2] == 7 || Mundo[Jugador.pos_y + 3][Jugador.pos_x + 2] == 7) {
			return true;
		}
		break;
	}
	return false;
}
bool ColisionJugadorTutor(Jugador_t Jugador, Tutor_t& Tutor) {
	switch (Jugador.direccion) {
	case 1:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_x == Tutor.pos_x[i] || Jugador.pos_x == Tutor.pos_x[i] + 1 || Jugador.pos_x == Tutor.pos_x[i] + 2 ||
				 Jugador.pos_x == Tutor.pos_x[i] - 1 || Jugador.pos_x == Tutor.pos_x[i] - 2) &&
				 Jugador.pos_y == Tutor.pos_y[i] + 3) {
				if (Tutor.interaccion[i] == 0) { Tutor.interaccion[i] = 1; }
				return true;
			}
		}
		break;
	case 2:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_y == Tutor.pos_y[i] || 
				 Jugador.pos_y == Tutor.pos_y[i] + 1 || Jugador.pos_y == Tutor.pos_y[i] + 2 || Jugador.pos_y == Tutor.pos_y[i] + 3 ||
				 Jugador.pos_y == Tutor.pos_y[i] - 1 || Jugador.pos_y == Tutor.pos_y[i] - 2 || Jugador.pos_y == Tutor.pos_y[i] - 3) &&
				 Jugador.pos_x == Tutor.pos_x[i] + 2) {
				if (Tutor.interaccion[i] == 0) { Tutor.interaccion[i] = 1; }
				return true;
			}
		}
		break;
	case 3:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_x == Tutor.pos_x[i] || Jugador.pos_x == Tutor.pos_x[i] + 1 || Jugador.pos_x == Tutor.pos_x[i] + 2 ||
				 Jugador.pos_x == Tutor.pos_x[i] - 1 || Jugador.pos_x == Tutor.pos_x[i] - 2) &&
				 Jugador.pos_y == Tutor.pos_y[i] - 3) {
				if (Tutor.interaccion[i] == 0) { Tutor.interaccion[i] = 1; }
				return true;
			}
		}
		break;
	case 4:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_y == Tutor.pos_y[i] || 
				 Jugador.pos_y == Tutor.pos_y[i] + 1 || Jugador.pos_y == Tutor.pos_y[i] + 2 || Jugador.pos_y == Tutor.pos_y[i] + 3 ||
				 Jugador.pos_y == Tutor.pos_y[i] - 1 || Jugador.pos_y == Tutor.pos_y[i] - 2 || Jugador.pos_y == Tutor.pos_y[i] - 3) &&
				 Jugador.pos_x == Tutor.pos_x[i] - 2) {
				if (Tutor.interaccion[i] == 0) { Tutor.interaccion[i] = 1; }
				return true;
			}
		}
		break;
	}
	return false;
}
bool ColisionJugadorEnemigo(Jugador_t Jugador, Enemigo_t& Enemigo) {

	switch (Jugador.direccion) {
	case 1:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_x == Enemigo.pos_x[i] || 
				 Jugador.pos_x == Enemigo.pos_x[i] + 1 || Jugador.pos_x == Enemigo.pos_x[i] + 2 ||
				 Jugador.pos_x == Enemigo.pos_x[i] - 1 || Jugador.pos_x == Enemigo.pos_x[i] - 2) &&
				(Jugador.pos_y == Enemigo.pos_y[i] + 3 || Jugador.pos_y == Enemigo.pos_y[i] + 4) && Enemigo.vencido[i] == false) {
				if (Enemigo.interaccion[i] == 0) { 
					Enemigo.interaccion[i] = 1; 
				}
				return true;
			}
		}
		break;
	case 2:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_y == Enemigo.pos_y[i] || 
				 Jugador.pos_y == Enemigo.pos_y[i] + 1 || Jugador.pos_y == Enemigo.pos_y[i] + 2 || Jugador.pos_y == Enemigo.pos_y[i] + 3 ||
				 Jugador.pos_y == Enemigo.pos_y[i] - 1 || Jugador.pos_y == Enemigo.pos_y[i] - 2 || Jugador.pos_y == Enemigo.pos_y[i] - 3) &&
				(Jugador.pos_x == Enemigo.pos_x[i] + 2 || Jugador.pos_x == Enemigo.pos_x[i] + 3) && Enemigo.vencido[i] == false) {
				if (Enemigo.interaccion[i] == 0) {
					Enemigo.interaccion[i] = 1;
				}
				return true;
			}
		}
		break;
	case 3:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_x == Enemigo.pos_x[i] || 
				 Jugador.pos_x == Enemigo.pos_x[i] + 1 || Jugador.pos_x == Enemigo.pos_x[i] + 2 ||
				 Jugador.pos_x == Enemigo.pos_x[i] - 1 || Jugador.pos_x == Enemigo.pos_x[i] - 2) &&
				(Jugador.pos_y == Enemigo.pos_y[i] - 3 || Jugador.pos_y == Enemigo.pos_y[i] - 4) && Enemigo.vencido[i] == false) {
				if (Enemigo.interaccion[i] == 0) {
					Enemigo.interaccion[i] = 1;
				}
				return true;
			}
		}
		break;
	case 4:
		for (int i = 0; i < 3; ++i) {
			if ((Jugador.pos_y == Enemigo.pos_y[i] ||
				 Jugador.pos_y == Enemigo.pos_y[i] + 1 || Jugador.pos_y == Enemigo.pos_y[i] + 2 || Jugador.pos_y == Enemigo.pos_y[i] + 3 ||
				 Jugador.pos_y == Enemigo.pos_y[i] - 1 || Jugador.pos_y == Enemigo.pos_y[i] - 2 || Jugador.pos_y == Enemigo.pos_y[i] - 3) &&
				(Jugador.pos_x == Enemigo.pos_x[i] - 2 || Jugador.pos_x == Enemigo.pos_x[i] - 3) && Enemigo.vencido[i] == false) {
				if (Enemigo.interaccion[i] == 0) { 
					Enemigo.interaccion[i] = 1; 
				}
				return true;
			}
		}
		break;
	}
	return false;
}
bool ColisionJugadorMeta(Jugador_t Jugador, Meta_t Meta, bool& mundo_superado, int mundo_actual) {
	switch (mundo_actual) {
	case 1:
		for (int i = 0; i < 4; ++i) {
			if (Jugador.pos_y + 3 == Meta.pos_y && Jugador.pos_x == Meta.pos_x + i) {
				mundo_superado = true;
				return true;
			}
		}
		break;
	case 2:
		for (int i = 0; i < 5; ++i) {
			if ((Jugador.pos_y - i == Meta.pos_y || Jugador.pos_y + i == Meta.pos_y) && Jugador.pos_x + 2  == Meta.pos_x) {
				mundo_superado = true;
				return true;	
			}
		}
		break;
	case 3:
		for (int i = 0; i < 4; ++i) {
			if (Jugador.pos_y + 3 == Meta.pos_y && Jugador.pos_x == Meta.pos_x + i) {
				mundo_superado = true;
				return true;
			}
		}
		break;
	default:
		return false;
		break;
	}
}
void LimpiarJugador(Jugador_t Jugador, short Mundo[24][120]) {
	PintarMundo(Mundo, Jugador.pos_y, Jugador.pos_x);
	Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y); cout << "   ";
	Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 1); cout << "   ";
	Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 2); cout << "   ";
	Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 3); cout << "   ";
}
void MovimientoJugador(Jugador_t& Jugador, Tutor_t& Tutor, Enemigo_t& Enemigo, Meta_t Meta,
					   short Mundo[24][120], bool& mundo_superado, int mundo_actual) {

	int tecla = 0;
	keyboardHandle(tecla);

	switch (tecla) {
	case 1:
		LimpiarJugador(Jugador, Mundo);
		--Jugador.pos_y;
		Jugador.direccion = 1;
		if (ColisionJugadorMapa(Jugador, Mundo) == true ||
			ColisionJugadorTutor(Jugador, Tutor) == true ||
			ColisionJugadorEnemigo(Jugador, Enemigo) == true ||
			ColisionJugadorMeta(Jugador, Meta, mundo_superado, mundo_actual) == true) {
			++Jugador.pos_y;
		}
		break;
	case 2:
		LimpiarJugador(Jugador, Mundo);
		--Jugador.pos_x;
		Jugador.direccion = 2;
		if (ColisionJugadorMapa(Jugador, Mundo) == true ||
			ColisionJugadorTutor(Jugador, Tutor) == true ||
			ColisionJugadorEnemigo(Jugador, Enemigo) == true ||
			ColisionJugadorMeta(Jugador, Meta, mundo_superado, mundo_actual) == true) {
			++Jugador.pos_x;
		}
		break;
	case 3:
		LimpiarJugador(Jugador, Mundo);
		++Jugador.pos_y;
		Jugador.direccion = 3;
		if (ColisionJugadorMapa(Jugador, Mundo) == true ||
			ColisionJugadorTutor(Jugador, Tutor) == true ||
			ColisionJugadorEnemigo(Jugador, Enemigo) == true ||
			ColisionJugadorMeta(Jugador, Meta, mundo_superado, mundo_actual) == true) {
			--Jugador.pos_y;
		}
		break;
	case 4:
		LimpiarJugador(Jugador, Mundo);
		++Jugador.pos_x;
		Jugador.direccion = 4;
		if (ColisionJugadorMapa(Jugador, Mundo) == true ||
			ColisionJugadorTutor(Jugador, Tutor) == true ||
			ColisionJugadorEnemigo(Jugador, Enemigo) == true ||
			ColisionJugadorMeta(Jugador, Meta, mundo_superado, mundo_actual) == true) {
			--Jugador.pos_x;
		}
		break;
	default:
		LimpiarJugador(Jugador, Mundo);
		ColisionEnemigoJugador(Jugador, Enemigo);
		break;
	}
	Jugador.direccion = 0;

}
void ImprimirJugador(Jugador_t& Jugador, short Mundo[24][120]) {

	// Imprimir al jugador
	PintarMundo(Mundo, Jugador.pos_y, Jugador.pos_x);
	if (Jugador.items_recibidos[0] == true || Jugador.items_recibidos[1] == true || Jugador.items_recibidos[2] == true) {
		asignarColor(0);
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y); cout << " " << (char)220 << " ";
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 1); cout << (char)218 << (char)197 << (char)191;
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 2); cout << (char)179 << (char)179 << (char)241;
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 3); cout << " " << (char)186 << " ";
	}
	else {
		asignarColor(0);
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y); cout << " " << (char)220 << " ";
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 1); cout << (char)218 << (char)197 << (char)191;
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 2); cout << (char)179 << (char)179 << (char)179;
		Console::SetCursorPosition(Jugador.pos_x, Jugador.pos_y + 3); cout << " " << (char)186 << " ";
	}
}

//Funciones del enemigo
void ColisionEnemigoJugador(Jugador_t& Jugador, Enemigo_t& Enemigo) {

	for (int i = 0; i < 3; ++i) {
		if ((Jugador.pos_x == Enemigo.pos_x[i] || 
			 Jugador.pos_x == Enemigo.pos_x[i] + 1 || Jugador.pos_x == Enemigo.pos_x[i] + 2 ||
			 Jugador.pos_x == Enemigo.pos_x[i] - 1 || Jugador.pos_x == Enemigo.pos_x[i] - 2) &&
			(Jugador.pos_y == Enemigo.pos_y[i] + 4 || Jugador.pos_y == Enemigo.pos_y[i] + 3) && Enemigo.vencido[i] == false) {
			if (Enemigo.interaccion[i] == 0) { 
				Enemigo.interaccion[i] = 1; 
			}
			++Jugador.pos_y;
		}
		else if ((Jugador.pos_y == Enemigo.pos_y[i] || 
			      Jugador.pos_y == Enemigo.pos_y[i] + 1 || Jugador.pos_y == Enemigo.pos_y[i] + 2 || Jugador.pos_y == Enemigo.pos_y[i] + 3 ||
			      Jugador.pos_y == Enemigo.pos_y[i] - 1 || Jugador.pos_y == Enemigo.pos_y[i] - 2 || Jugador.pos_y == Enemigo.pos_y[i] - 3) &&
				 (Jugador.pos_x == Enemigo.pos_x[i] + 3 || Jugador.pos_x == Enemigo.pos_x[i] + 2) && Enemigo.vencido[i] == false) {
			if (Enemigo.interaccion[i] == 0) { 
				Enemigo.interaccion[i] = 1; 
			}
			++Jugador.pos_x;
		}
		else if ((Jugador.pos_x == Enemigo.pos_x[i] ||
			      Jugador.pos_x == Enemigo.pos_x[i] + 1 || Jugador.pos_x == Enemigo.pos_x[i] + 2 ||
			      Jugador.pos_x == Enemigo.pos_x[i] - 1 || Jugador.pos_x == Enemigo.pos_x[i] - 2) &&
				 (Jugador.pos_y == Enemigo.pos_y[i] - 4 || Jugador.pos_y == Enemigo.pos_y[i] - 3) && Enemigo.vencido[i] == false) {
			if (Enemigo.interaccion[i] == 0) { 
				Enemigo.interaccion[i] = 1; 
			}
			--Jugador.pos_y;
		}
		else if ((Jugador.pos_y == Enemigo.pos_y[i] ||
				  Jugador.pos_y == Enemigo.pos_y[i] + 1 || Jugador.pos_y == Enemigo.pos_y[i] + 2 || Jugador.pos_y == Enemigo.pos_y[i] + 3 ||
				  Jugador.pos_y == Enemigo.pos_y[i] - 1 || Jugador.pos_y == Enemigo.pos_y[i] - 2 || Jugador.pos_y == Enemigo.pos_y[i] - 3) &&
				 (Jugador.pos_x == Enemigo.pos_x[i] - 3 || Jugador.pos_x == Enemigo.pos_x[i] - 2) && Enemigo.vencido[i] == false) {
			if (Enemigo.interaccion[i] == 0) { 
				Enemigo.interaccion[i] = 1; 
			}
			--Jugador.pos_x;
		}
	}

}
void InteraccionEnemigos_Mundo1(Jugador_t& Jugador, Enemigo_t& Enemigo, Partida_t& Partida, int& PuntajesDeUsuarios, short Mundo[24][120]) {

	int var1;
	int var2;
	int resultado;

	if (Enemigo.interaccion[0] == 1) {
		if (Jugador.items_recibidos[0] == true) {

			do {
				var1 = rand() % 9 + 1;
				var2 = rand() % 9 + 1;
				resultado = var1 + var2;
			} while (resultado >= 10 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo1_Mundo1(var1, var2, resultado)) {
				PuntajesDeUsuarios += 20;
				Enemigo.interaccion[0] = 2;
				Enemigo.vencido[0] = true;
				Jugador.items_recibidos[0] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[0] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 2;
				Jugador.pos_y = 1;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[0] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 2;
			Jugador.pos_y = 1;
		}
	}
	else if (Enemigo.interaccion[1] == 1) {
		if (Jugador.items_recibidos[1] == true) {

			do {
				var1 = rand() % 90 + 10;
				var2 = rand() % 90 + 10;
				resultado = var1 + var2;
			} while (resultado >= 100 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo2_Mundo1(var1, var2, resultado)) {
				PuntajesDeUsuarios += 40;
				Enemigo.interaccion[1] = 2;
				Enemigo.vencido[1] = true;
				Jugador.items_recibidos[1] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[1] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 2;
				Jugador.pos_y = 1;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[1] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 2;
			Jugador.pos_y = 1;
		}
	}
	else if (Enemigo.interaccion[2] == 1) {
		if (Jugador.items_recibidos[2] == true) {

			do {
				var1 = rand() % 900 + 100;
				var2 = rand() % 900 + 100;
				resultado = var1 + var2;
			} while (resultado >= 1000 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo3_Mundo1(var1, var2, resultado)) {
				PuntajesDeUsuarios += 80;
				Enemigo.interaccion[2] = 2;
				Enemigo.vencido[2] = true;
				Jugador.items_recibidos[2] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[2] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 2;
				Jugador.pos_y = 1;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[2] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 2;
			Jugador.pos_y = 1;
		}
	}
}
void InteraccionEnemigos_Mundo2(Jugador_t& Jugador, Enemigo_t& Enemigo, Partida_t& Partida, int& PuntajesDeUsuarios, short Mundo[24][120]) {

	int var1;
	int var2;
	int resultado;

	if (Enemigo.interaccion[0] == 1) {
		if (Jugador.items_recibidos[0] == true) {

			do {
				var1 = rand() % 9 + 1;
				var2 = rand() % 9 + 1;
				resultado = var1 - var2;
			} while (var1 <= var2 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo1_Mundo2(var1, var2, resultado)) {
				PuntajesDeUsuarios += 40;
				Enemigo.interaccion[0] = 2;
				Enemigo.vencido[0] = true;
				Jugador.items_recibidos[0] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[0] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 105;
				Jugador.pos_y = 20;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[0] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 105;
			Jugador.pos_y = 20;
		}
	}
	else if (Enemigo.interaccion[1] == 1) {
		if (Jugador.items_recibidos[1] == true) {

			do {
				var1 = rand() % 90 + 10;
				var2 = rand() % 90 + 10;
				resultado = var1 - var2;
			} while (var1 <= var2 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo2_Mundo2(var1, var2, resultado)) {
				PuntajesDeUsuarios += 80;
				Enemigo.interaccion[1] = 2;
				Enemigo.vencido[1] = true;
				Jugador.items_recibidos[1] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[1] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 105;
				Jugador.pos_y = 20;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[1] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 105;
			Jugador.pos_y = 20;
		}
	}
	else if (Enemigo.interaccion[2] == 1) {
		if (Jugador.items_recibidos[2] == true) {

			do {
				var1 = rand() % 900 + 100;
				var2 = rand() % 900 + 100;
				resultado = var1 - var2;
			} while (var1 <= var2 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo3_Mundo2(var1, var2, resultado)) {
				PuntajesDeUsuarios += 120;
				Enemigo.interaccion[2] = 2;
				Enemigo.vencido[2] = true;
				Jugador.items_recibidos[2] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[2] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 105;
				Jugador.pos_y = 20;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[2] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 105;
			Jugador.pos_y = 20;
		}
	}
}
void InteraccionEnemigos_Mundo3(Jugador_t& Jugador, Enemigo_t& Enemigo, Partida_t& Partida, int& PuntajesDeUsuarios, short Mundo[24][120]) {

	int var1;
	int var2;
	int var3;
	int resultado;

	if (Enemigo.interaccion[0] == 1) {
		if (Jugador.items_recibidos[0] == true) {

			do {
				var1 = rand() % 9 + 1;
				var2 = rand() % 9 + 1;
				resultado = var2 - var1;
			} while (var1 >= var2 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo1_Mundo3(var1, var2, resultado)) {
				PuntajesDeUsuarios += 120;
				Enemigo.interaccion[0] = 2;
				Enemigo.vencido[0] = true;
				Jugador.items_recibidos[0] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[0] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 9;
				Jugador.pos_y = 5;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[0] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 9;
			Jugador.pos_y = 5;
		}
	}
	else if (Enemigo.interaccion[1] == 1) {
		if (Jugador.items_recibidos[1] == true) {

			do {
				var1 = rand() % 90 + 10;
				var2 = rand() % 90 + 10;
				resultado = var1 + var2;
			} while (resultado >= 100 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo2_Mundo3(var1, var2, resultado)) {
				PuntajesDeUsuarios += 150;
				Enemigo.interaccion[1] = 2;
				Enemigo.vencido[1] = true;
				Jugador.items_recibidos[1] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[1] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 9;
				Jugador.pos_y = 5;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[1] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 9;
			Jugador.pos_y = 5;
		}
	}
	else if (Enemigo.interaccion[2] == 1) {
		if (Jugador.items_recibidos[2] == true) {

			do {
				var1 = rand() % 900 + 100;
				var2 = rand() % 900 + 100;
				var3 = rand() % 900 + 100;
				resultado = var1 + var2 + var3;
			} while (resultado >= 1000 || var1 == resultado || var2 == resultado);

			if (Msg_Enemigo3_Mundo3(var1, var2, var3, resultado)) {
				PuntajesDeUsuarios += 180;
				Enemigo.interaccion[2] = 2;
				Enemigo.vencido[2] = true;
				Jugador.items_recibidos[2] = false;
			}
			else {
				PuntajesDeUsuarios -= 20;
				Enemigo.interaccion[2] = 0;
				--Partida.vidas_restantes;
				LimpiarJugador(Jugador, Mundo);
				Jugador.pos_x = 9;
				Jugador.pos_y = 5;
			}
		}
		else {
			PuntajesDeUsuarios -= 20;
			Enemigo.interaccion[2] = 0;
			--Partida.vidas_restantes;
			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 9;
			Jugador.pos_y = 5;
		}
	}

}
void LimpiarEnemigo(Enemigo_t& Enemigo, short Mundo[24][120]) {
	for (int i = 0; i < 3; ++i) {
		if (Enemigo.vencido[i] == 0) {
			PintarMundo(Mundo, Enemigo.pos_y[i], Enemigo.pos_x[i]);
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i]); cout << "   ";
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 1); cout << "   ";
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 2); cout << "   ";
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 3); cout << "   ";
		}
		else if (Enemigo.vencido[i] == 1) {
			PintarMundo(Mundo, Enemigo.pos_y[i], Enemigo.pos_x[i]);
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i]); cout << "   ";
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 1); cout << "   ";
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 2); cout << "   ";
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 3); cout << "   ";
			Enemigo.vencido[i] = 2;
		}
	}
}
void MovimientoEnemigo_Mundo1(Enemigo_t& Enemigo) {
	if (Enemigo.mov_x[0] > -4 && Enemigo.mov_max[0] == false) {
		--Enemigo.pos_x[0];
		--Enemigo.mov_x[0];
	}
	else {
		Enemigo.mov_max[0] = true;
		++Enemigo.pos_x[0];
		++Enemigo.mov_x[0];
		if (Enemigo.mov_x[0] == 0) {
			Enemigo.mov_max[0] = false;
		}
	}

	if (Enemigo.mov_x[1] > -5 && Enemigo.mov_max[1] == false) {
		--Enemigo.pos_x[1];
		--Enemigo.mov_x[1];
	}
	else {
		Enemigo.mov_max[1] = true;
		++Enemigo.pos_x[1]; ++Enemigo.mov_x[1];
		if (Enemigo.mov_x[1] == 0) {
			Enemigo.mov_max[1] = false;
		}
	}

	if (Enemigo.mov_x[2] > -4 && Enemigo.mov_max[2] == false) {
		--Enemigo.pos_x[2];
		--Enemigo.mov_x[2];
	}
	else {
		Enemigo.mov_max[2] = true;
		++Enemigo.pos_x[2];
		++Enemigo.mov_x[2];
		if (Enemigo.mov_x[2] == 6) {
			Enemigo.mov_max[2] = false;
		}
	}
}
void MovimientoEnemigo_Mundo2(Enemigo_t& Enemigo) {
	if (Enemigo.mov_x[0] > -4 && Enemigo.mov_max[0] == false) {
		--Enemigo.pos_x[0];
		--Enemigo.mov_x[0];
	}
	else {
		Enemigo.mov_max[0] = true;
		++Enemigo.pos_x[0]; ++Enemigo.mov_x[0];
		if (Enemigo.mov_x[0] == 0) {
			Enemigo.mov_max[0] = false;
		}
	}

	if (Enemigo.mov_x[1] > -5 && Enemigo.mov_max[1] == false) {
		--Enemigo.pos_x[1];
		--Enemigo.mov_x[1];
	}
	else {
		Enemigo.mov_max[1] = true;
		++Enemigo.pos_x[1]; ++Enemigo.mov_x[1];
		if (Enemigo.mov_x[1] == 0) {
			Enemigo.mov_max[1] = false;
		}
	}

	if (Enemigo.mov_x[2] > -4 && Enemigo.mov_max[2] == false) {
		--Enemigo.pos_x[2];
		--Enemigo.mov_x[2];
	}
	else {
		Enemigo.mov_max[2] = true;
		++Enemigo.pos_x[2]; ++Enemigo.mov_x[2];
		if (Enemigo.mov_x[2] == 6) {
			Enemigo.mov_max[2] = false;
		}
	}
}
void MovimientoEnemigo_Mundo3(Enemigo_t& Enemigo) {
	if (Enemigo.mov_x[0] > -4 && Enemigo.mov_max[0] == false) {
		--Enemigo.pos_x[0];
		--Enemigo.mov_x[0];
	}
	else {
		Enemigo.mov_max[0] = true;
		++Enemigo.pos_x[0];
		++Enemigo.mov_x[0];
		if (Enemigo.mov_x[0] == 0) {
			Enemigo.mov_max[0] = false;
		}
	}

	if (Enemigo.mov_x[1] > -5 && Enemigo.mov_max[1] == false) {
		--Enemigo.pos_x[1];
		--Enemigo.mov_x[1];
	}
	else {
		Enemigo.mov_max[1] = true;
		++Enemigo.pos_x[1];
		++Enemigo.mov_x[1];
		if (Enemigo.mov_x[1] == 0) {
			Enemigo.mov_max[1] = false;
		}
	}

	if (Enemigo.mov_x[2] > -4 && Enemigo.mov_max[2] == false) {
		--Enemigo.pos_x[2];
		--Enemigo.mov_x[2];
	}
	else {
		Enemigo.mov_max[2] = true;
		++Enemigo.pos_x[2];
		++Enemigo.mov_x[2];
		if (Enemigo.mov_x[2] == 6) {
			Enemigo.mov_max[2] = false;
		}
	}
}
void ImprimirEnemigo(Enemigo_t Enemigo, short Mundo[24][120]) {

	for (int i = 0; i < 3; ++i) {

		if (Enemigo.vencido[i] == 0) {
			asignarColor(9);
			PintarMundo(Mundo, Enemigo.pos_y[i], Enemigo.pos_x[i]);
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i]); cout << " " << (char)220 << " ";
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 1); cout << (char)201 << (char)206 << (char)187;
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 2); cout << (char)179 << (char)179 << (char)179;
			Console::SetCursorPosition(Enemigo.pos_x[i], Enemigo.pos_y[i] + 3); cout << " " << (char)186 << " ";
		}

	}

}

//Funciones del tutor
void ImprimirTutor(Tutor_t Tutor, short Mundo[24][120]) {
	for (int i = 0; i < 3; ++i) {
		PintarMundo(Mundo, Tutor.pos_y[i], Tutor.pos_x[i]);
		asignarColor(13);
		Console::SetCursorPosition(Tutor.pos_x[i], Tutor.pos_y[i]); cout << " " << (char)157 << " ";
		Console::SetCursorPosition(Tutor.pos_x[i], Tutor.pos_y[i] + 1); cout << (char)201 << (char)197 << (char)187;
		Console::SetCursorPosition(Tutor.pos_x[i], Tutor.pos_y[i] + 2); cout << (char)179 << (char)186 << (char)179;
		Console::SetCursorPosition(Tutor.pos_x[i], Tutor.pos_y[i] + 3); cout << " " << (char)186 << " ";
	}
}
void InteraccionTutores_Mundo1(Jugador_t& Jugador, Tutor_t& Tutor, int& progreso_aprendizaje, int& PuntajesDeUsuarios){
	if (Tutor.interaccion[0] == 1) {

		Msg_Tutor1_Mundo1();
		progreso_aprendizaje += 2;
		PuntajesDeUsuarios += 20;
		Jugador.items_recibidos[0] = true;
		Tutor.interaccion[0] = 2;

	}
	else if (Tutor.interaccion[1] == 1) {

		Msg_Tutor2_Mundo1();
		progreso_aprendizaje += 8;
		PuntajesDeUsuarios += 30;
		Jugador.items_recibidos[1] = true;
		Tutor.interaccion[1] = 2;

	}
	else if (Tutor.interaccion[2] == 1) {

		Msg_Tutor3_Mundo1();
		progreso_aprendizaje += 10;
		PuntajesDeUsuarios += 40;
		Jugador.items_recibidos[2] = true;
		Tutor.interaccion[2] = 2;

	}
}
void InteraccionTutores_Mundo2(Jugador_t& Jugador, Tutor_t& Tutor, int& progreso_aprendizaje, int& PuntajesDeUsuarios) {
	if (Tutor.interaccion[0] == 1) {

		Msg_Tutor1_Mundo2();
		progreso_aprendizaje += 10;
		PuntajesDeUsuarios += 50;
		Jugador.items_recibidos[0] = true;
		Tutor.interaccion[0] = 2;

	}
	else if (Tutor.interaccion[1] == 1) {

		Msg_Tutor2_Mundo2();
		progreso_aprendizaje += 10;
		PuntajesDeUsuarios += 60;
		Jugador.items_recibidos[1] = true;
		Tutor.interaccion[1] = 2;

	}
	else if (Tutor.interaccion[2] == 1) {

		Msg_Tutor3_Mundo2();
		progreso_aprendizaje += 15;
		PuntajesDeUsuarios += 70;
		Jugador.items_recibidos[2] = true;
		Tutor.interaccion[2] = 2;

	}
}
void InteraccionTutores_Mundo3(Jugador_t& Jugador, Tutor_t& Tutor, int& progreso_aprendizaje, int& PuntajesDeUsuarios) {
	if (Tutor.interaccion[0] == 1) {

		Msg_Tutor1_Mundo3();
		progreso_aprendizaje += 10;
		PuntajesDeUsuarios += 80;
		Jugador.items_recibidos[0] = true;
		Tutor.interaccion[0] = 2;

	}
	else if (Tutor.interaccion[1] == 1) {

		Msg_Tutor2_Mundo3();
		progreso_aprendizaje += 15;
		PuntajesDeUsuarios += 90;
		Jugador.items_recibidos[1] = true;
		Tutor.interaccion[1] = 2;

	}
	else if (Tutor.interaccion[2] == 1) {

		Msg_Tutor3_Mundo3();
		progreso_aprendizaje += 20;
		PuntajesDeUsuarios += 100;
		Jugador.items_recibidos[2] = true;
		Tutor.interaccion[2] = 2;

	}
}

//Funcion adicional de portal
void ImprimirPortal(Portal_t Portal, short Mundo[24][120]) {

	for (int i = 0; i < 2; ++i) {
		PintarMundo(Mundo, Portal.pos_y[i], Portal.pos_x[i]);
		asignarColor(5);
		Console::SetCursorPosition(Portal.pos_x[i], Portal.pos_y[i]); cout << " @@ ";
		Console::SetCursorPosition(Portal.pos_x[i], Portal.pos_y[i] + 1); cout << "@@@@";
		Console::SetCursorPosition(Portal.pos_x[i], Portal.pos_y[i] + 2); cout << "@@@@";
		Console::SetCursorPosition(Portal.pos_x[i], Portal.pos_y[i] + 3); cout << " @@ ";
	}

}
void FuncionAdicionalPortal(Jugador_t& Jugador, Portal_t Portal, short Mundo[24][120], int mundo_actual) {

	switch (mundo_actual) {
	case 1:
		if (Jugador.pos_x + 2 == Portal.pos_x[0] && Jugador.pos_y == Portal.pos_y[0]) {

			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 64;
			Jugador.pos_y = 16;

		}
		else if ((Jugador.pos_x == Portal.pos_x[1] || Jugador.pos_x - 1 == Portal.pos_x[1] || Jugador.pos_x - 2 == Portal.pos_x[1]) &&
			Jugador.pos_y + 3 == Portal.pos_y[1]) {

			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 8;
			Jugador.pos_y = 11;

		}
		break;
	case 2:
		if (Jugador.pos_x - 3 == Portal.pos_x[0] &&
			(Jugador.pos_y == Portal.pos_y[0] || Jugador.pos_y + 1 == Portal.pos_y[0] ||
				Jugador.pos_y + 2 == Portal.pos_y[0] || Jugador.pos_y + 3 == Portal.pos_y[0])) {

			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 75;
			Jugador.pos_y = 4;

		}
		else if ((Jugador.pos_x == Portal.pos_x[1] ||
			Jugador.pos_x + 1 == Portal.pos_x[1] || Jugador.pos_x + 2 == Portal.pos_x[1] || Jugador.pos_x + 3 == Portal.pos_x[1] ||
			Jugador.pos_x - 1 == Portal.pos_x[1] || Jugador.pos_x - 2 == Portal.pos_x[1] || Jugador.pos_x - 3 == Portal.pos_x[1]) &&
			Jugador.pos_y - 3 == Portal.pos_y[1]) {

			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 113;
			Jugador.pos_y = 9;

		}
		break;
	case 3:
		if (((Jugador.pos_x - 1 == Portal.pos_x[0] || Jugador.pos_x - 2 == Portal.pos_x[0] || Jugador.pos_x - 3 == Portal.pos_x[0] ||
			Jugador.pos_x == Portal.pos_x[0] ||
			Jugador.pos_x + 1 == Portal.pos_x[0] || Jugador.pos_x + 2 == Portal.pos_x[0]) && Jugador.pos_y + 3 == Portal.pos_y[0]) ||
		   ((Jugador.pos_y == Portal.pos_y[0] || Jugador.pos_y + 1 == Portal.pos_y[0] || Jugador.pos_y + 2 == Portal.pos_y[0] || 
			Jugador.pos_y + 3 == Portal.pos_y[0]) && Jugador.pos_x + 2 == Portal.pos_x[0])) {

			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 115;
			Jugador.pos_y = 0;

		}
		else if (Jugador.pos_x - 3 == Portal.pos_x[1] && Jugador.pos_y == Portal.pos_y[1]) {

			LimpiarJugador(Jugador, Mundo);
			Jugador.pos_x = 3;
			Jugador.pos_y = 9;

		}
		break;
	}
}

//Funciones end-game
void ventanaGanaste(int PuntajesDeUsuarios) {
	
	Console::Clear();
	asignarColor(11);
	Console::SetCursorPosition(1, 10); cout << "OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO OOOO        OOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO";
	Console::SetCursorPosition(1, 11); cout << "OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO OOOOO       OOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO";
	Console::SetCursorPosition(1, 12); cout << "OOOO             OOOO        OOOO OOOOOOO     OOOO OOOO        OOOO OOOO                   OOOO       OOOO            ";
	Console::SetCursorPosition(1, 13); cout << "OOOO             OOOO        OOOO OOOOOOOO    OOOO OOOO        OOOO OOOO                   OOOO       OOOO            ";
	Console::SetCursorPosition(1, 14); cout << "OOOO   OOOOOOOOO OOOOOOOOOOOOOOOO OOOO OOOO   OOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO       OOOO       OOOOOOOOOOOOOOOO";
	Console::SetCursorPosition(1, 15); cout << "OOOO   OOOOOOOOO OOOOOOOOOOOOOOOO OOOO   OOOO OOOO OOOOOOOOOOOOOOOO OOOOOOOOOOOOOOOO       OOOO       OOOOOOOOOOOOOOOO";
	Console::SetCursorPosition(1, 16); cout << "OOOO        OOOO OOOO        OOOO OOOO    OOOOOOOO OOOO        OOOO             OOOO       OOOO       OOOO            ";
	Console::SetCursorPosition(1, 17); cout << "OOOO        OOOO OOOO        OOOO OOOO     OOOOOOO OOOO        OOOO             OOOO       OOOO       OOOO            ";
	Console::SetCursorPosition(1, 18); cout << "OOOOOOOOOOOOOOOO OOOO        OOOO OOOO       OOOOO OOOO        OOOO OOOOOOOOOOOOOOOO       OOOO       OOOOOOOOOOOOOOOO";
	Console::SetCursorPosition(1, 19); cout << "OOOOOOOOOOOOOOOO OOOO        OOOO OOOO        OOOO OOOO        OOOO OOOOOOOOOOOOOOOO       OOOO       OOOOOOOOOOOOOOOO";
	asignarColor(12);
	Console::SetCursorPosition(1, 21); cout << "                                                 PUNTAJE OBTENIDO: " << PuntajesDeUsuarios;
	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
}
void ventanaPerdiste(int PuntajesDeUsuarios) {

	Console::Clear();
	asignarColor(9);
	Console::SetCursorPosition(4, 10); cout << "OOOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOO    OOOOOOOOOO    OOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOOOOO";
	Console::SetCursorPosition(4, 11); cout << "OOOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOO    OOOOOOOOOO    OOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOOOOO";
	Console::SetCursorPosition(4, 12); cout << "OOOO       OOO OOO           OOO       OOO OOO       OOO      OOO      OOO                OOO      OOO          ";
	Console::SetCursorPosition(4, 13); cout << "OOOO       OOO OOO           OOO       OOO OOO       OOO      OOO      OOO                OOO      OOO          ";
	Console::SetCursorPosition(4, 14); cout << "OOOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOO    OOO       OOO      OOO      OOOOOOOOOOOOO      OOO      OOOOOOOOOOOOO";
	Console::SetCursorPosition(4, 15); cout << "OOOOOOOOOOOOOO OOOOOOOOOOOOO OOOOOOOOOO    OOO       OOO      OOO      OOOOOOOOOOOOO      OOO      OOOOOOOOOOOOO";
	Console::SetCursorPosition(4, 16); cout << "OOOO           OOO           OOO       OOO OOO       OOO      OOO                OOO      OOO      OOO          ";
	Console::SetCursorPosition(4, 17); cout << "OOOO           OOO           OOO       OOO OOO       OOO      OOO                OOO      OOO      OOO          ";
	Console::SetCursorPosition(4, 18); cout << "OOOO           OOOOOOOOOOOOO OOO       OOO OOOOOOOOOO    OOOOOOOOOOOOO OOOOOOOOOOOOO      OOO      OOOOOOOOOOOOO";
	Console::SetCursorPosition(4, 19); cout << "OOOO           OOOOOOOOOOOOO OOO       OOO OOOOOOOOOO    OOOOOOOOOOOOO OOOOOOOOOOOOO      OOO      OOOOOOOOOOOOO";
	asignarColor(12);
	Console::SetCursorPosition(1, 21); cout << "                                                 PUNTAJE OBTENIDO: " << PuntajesDeUsuarios;
	int tecla = 0;
	for (;;) {
		keyboardHandle(tecla);
		if (tecla == 5) { break; }
	}
}

#endif
#include "Biblioteca_v2.h"

int main() {

	srand(time(nullptr));
	Console::CursorVisible = false;

	//Cantidad maxima de jugadores
	int jugadores_max = 20;

	//Arreglos de almacenamiento de datos
	string* NombresDeUsuarios = new string[jugadores_max];
	int* PuntajesDeUsuarios = new int[jugadores_max];
	int partida_actual = 0;

	//El programa solo admitira como maximo 20 jugadores por ejecucion
	for (int i = 0; i < jugadores_max; ++i) { NombresDeUsuarios[i] = "-"; }
	for (int i = 0; i < jugadores_max; ++i) { PuntajesDeUsuarios[i] = 0; }

	//Imprime mensaje cuando se inicia el programa por primera vez
	imprimirMarco();
	ventanaInicio();

	int opcion_actual = 1;
	bool salida = false;

	while (salida == false) {

		//Funcion que modifica la opcion actual
		ventanaInteractiva(opcion_actual);

		switch (opcion_actual) {
		case 1: ventanaNuevaPartida(NombresDeUsuarios, partida_actual); break;
		case 2: ventanaInstrucciones(); break;
		case 3: ventanaHistoria(); break;
		case 4: ventanaRankings(NombresDeUsuarios, PuntajesDeUsuarios); break;
		case 5: ventanaCreadores(); break;
		case 6: ventanaSalir(salida); break;
		}

		if (opcion_actual == 1) {
			PartidaEnJuego(PuntajesDeUsuarios[partida_actual - 1], NombresDeUsuarios[partida_actual - 1]);
		}

	}

	delete[]NombresDeUsuarios;
	delete[]PuntajesDeUsuarios;

	return EXIT_SUCCESS;
}

